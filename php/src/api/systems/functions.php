<?php


function limit_text($text, $limit) {

    if (str_word_count($text, 0) > $limit) {
        $text = html_entity_decode(strip_tags($text));
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function tipe_text($text) {

    $text = strip_tags($text);
    // $words = str_word_count($text, 2);

    return $text;
}

//function random($length)
//{
//    $data = 'ABCDEFGHIJKLMNOPQRSTU1234567890';
//    $string = '';
//    for($i = 0; $i < $length; $i++) {
//        $pos = rand(0, strlen($data)-1);
//        $string .= $data{$pos};
//    }
//    return $string;
//}
//
//function randomImage($length)
//{
//    $data = '1234567890';
//    $string = '';
//    for($i = 0; $i < $length; $i++) {
//        $pos = rand(0, strlen($data)-1);
//        $string .= $data{$pos};
//    }
//    return $string;
//}

function createImage($path, $filename, $id, $proporsional = false) {
    $newFileName = urlParsing($filename);
    $big = $path . $id . 'main_' . $newFileName;
    $thumb = $path . $id . 'thumb_'. $newFileName;
    if (file_exists($big) && file_exists($thumb)) {
        // unlink($big);
        // unlink($thumb);
    }
    $file = $path . $filename;

    smart_resize_image($file, $big, config('resize_image_width_big'), config('resize_image_height_big'), true , 90);
    smart_resize_image($file, $thumb, config('resize_image_width_thumb'), config('resize_image_height_thumb'), true , 90);

    // move_uploaded_file($file);
    return [
        'big' => $id . 'main_' . $newFileName,
        'thumb' => $id . 'thumb_' . $newFileName
    ];
}


function countArtikel($status,$id){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $db->select('count(*) as jumlah')
        ->from('artikel');

    if ($status == 'Penulis') {
        $db->where("created_by","=",$id);
    } else {
        $db->where("modified_by","=",$id);
    }
    return $db;
}

function getDescription($content){
    $awal = explode("<br />", $content);
    return strip_tags($awal[5]);
}

function getPortofolioap(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("kategori", '=', 1)
        ->orderBy("artikel.id DESC")
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }
    return $contentbox;
}
function getPortofolioperum(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("kategori", '=', 2)
        ->orderBy("artikel.id DESC")
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

    return $contentbox;
}
function latestPortofolio(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->limit(5)
        ->orderBy("artikel.id DESC")
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

    return $contentbox;
}

//fungsi baru
function gambar_pertama($string, $default = "app/img/no-image-article.png") {
    // preg_match('@<img.+src="(.)".>@Uims', $string, $matches);

    preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $string, $image);
    // $src = $matches ? $matches[1] : site_url() .$default;
    return @$image['src'];
    // return $src;
}

function getSettingweb(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $get_sw = $db->select("*")
        ->from("setting")
        ->findAll();


    return $get_sw;
}
function getnoWa(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $get_wa = $db->select("*")
        ->from("setting")
        ->find();
    $panjang_no = strlen($get_wa->whatsapp)-1 ;

    if(substr($get_wa->whatsapp,0,1) == 0)
    {
        $no_wa = 62 . substr($get_wa->whatsapp,1,$panjang_no);
    }
    else
    {
        $no_wa =  $get_wa->whatsapp;
    }


    if(substr($get_wa->whatsapp,0,2) == 62)
    {
        $no_alias = 0 . substr($get_wa->whatsapp,2,$panjang_no);
    }
    else
    {
        $no_alias =  $get_wa->whatsapp;
    }

    $alias =  substr($no_alias,0,4) . "-" .  substr($no_alias,4,4) ."-". substr($no_alias,8,$panjang_no);




    $wa = [];
    $wa[0]->no_wa = $no_wa;
    $wa[0]->alias = $alias;


    return $wa;
}

function getProduk(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("m_produk")
        ->findAll();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $data = $db->select('*')->from('produk_spesifikasi')->where('produk_id', '=', $value->id)->find();
        $false = (object) '';
        $models[$key]['spek']    =  $data ? $data : $false ;
    }
//echo json_encode($models);
//    die();

    return $models;
}
function getProductDetail($nama){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("m_produk")
        ->where("alias", "=", $nama)
        ->findAll();

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $spek = $db->select('*')->from('produk_spesifikasi')->where('produk_id', '=', $value->id)->find();
        $slider = $db->select('*')->from('produk_slider')->where('produk_id', '=', $value->id)->findAll();
        $false = (object) '';
        $models[$key]['spek']    =  $spek ? $spek : $false ;
        $models[$key]['slider']    =  $slider ? $slider : $false ;

    }
//echo json_encode($models);
//    die();

    return $models;
}

function getProductDetailNama($nama){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("m_produk")
        ->where("alias", "=", $nama)
        ->find();


    return $models->nama;
}

function getGallery(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("galeri")
        ->where("galeri_kategori_id", "=",2)
        ->orderBy("id DESC")
        ->findAll();

    return $models;
}

function getProdukHome(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("galeri")
        ->where("galeri_kategori_id", "=",5) //Gallery Produk
        ->orderBy("id DESC")
        ->findAll();


//echo json_encode($models);
//die();

    return $models;
}

function getPrestasiHome(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("galeri")
        ->where("galeri_kategori_id", "=",7) //Gallery Prestasi
        ->orderBy("id DESC")
        ->findAll();

//
//echo json_encode($models);
//die();

    return $models;
}

function getSlider(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("galeri")
        ->where("galeri_kategori_id", "=",4) //Gallery Produk
        ->limit(3)
        ->findAll();
//    echo json_encode($models);
//    die();


    return $models;
}


function getAllGallery(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("galeri")
        ->where("galeri_kategori_id", "=",3)
        ->findAll();
    return $models;
}

function getGalleryHome(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $models = $db->select("*")
        ->from("galeri")
        ->limit(3)
        ->findAll();

    foreach ($models as $key => $value) {
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $models;
}


function getArtikel(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->where("kategori", "=", 6)
        ->orderBy("artikel.id DESC")
        ->limit(3)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}

function getNextArtikel($id){

    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->where("kategori", "=", 1)
        ->where("id", "!=", $id)
        ->orderBy("artikel.id DESC")
        ->limit(3)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}


function getNextLegalInformation($id){

    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->where("kategori", "=", 7)
        ->where("id", "!=", $id)
        ->orderBy("artikel.id DESC")
        ->limit(3)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}

function print_die($data) {
    echo json_encode($data);
    die();
}

function getJadwal(){
    $today = date("l");
    $hari = [
        [
            "hari" => "Monday",
            "buka" => "Senin : 08.00 - 16.00"
        ],
        [
            "hari" => "Tuesday",
            "buka" => "Selasa : 08.00 - 16.00"
        ],
        [
            "hari" => "Wednesday",
            "buka" => "Rabu : 08.00 - 16.00"
        ],
        [
            "hari" => "Thursday",
            "buka" => "Kamis : 08.00 - 16.00"
        ],
        [
            "hari" => "Friday",
            "buka" => "Jum'at : 08.00 - 15.00"
        ],
        [
            "hari" => "Saturday",
            "buka" => "Sabtu : 08.00 - 12.00"
        ],
        [
            "hari" => "Sunday",
            "buka" => "Minggu : Tutup"
        ]
    ];
    foreach ($hari as $key => $value) {
        if ($today == $value['hari']) {
            $hari_ini = $value['buka'];
        }
    }
    return[
        'tudey' => $hari_ini,
        'list' => $hari,
    ];
}

function getAllArtikel(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("m_artikel.*,
    m_user.nama as penulis,
    m_kategori_artikel.nama AS kategori")
        ->from("m_artikel")
        ->join("left join","m_user","m_user.id = m_artikel.created_by")
        ->join("left join", "m_kategori_artikel", "m_artikel.kategori_artikel_id=m_kategori_artikel.id")
        ->where("publish", "=", 1)
        ->where("kategori_artikel_id", "=", 1)
        ->orderBy("m_artikel.id DESC")
        ->limit(4)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->title;
        $value->thumbnail = gambar_pertama($value->content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    print_die($contentbox);
    return $contentbox;
}

function getAllLegalInformation(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->where("kategori", "=", 7) // Legal Information
        ->orderBy("artikel.id DESC")
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}

function getArtikelProfile(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("kategori", '=', 3)
        ->orderBy("artikel.id DESC")
        ->limit(1)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}

function getArtikelMasterPlan(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("kategori", '=', 4)
        ->orderBy("artikel.id DESC")
        ->limit(1)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}

function getArtikelFasilitas(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("kategori", '=', 5)
        ->orderBy("artikel.id DESC")
        ->limit(1)
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
        $judul = $value->judul;
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->tgl_berita = date('d F Y', $value->created_at );
        $value->bulan_berita = date('F', $value->created_at );
        $value->created_at = date('d F Y', $value->created_at );
    }

//    echo json_encode($contentbox);
//    die();
    return $contentbox;
}

function getMeta($meta){
    if ($meta == "seo_title"){
        return "Desa Campurejo - Sambit Ponorogo";
    }elseif ($meta == "seo_keywords"){
        return "desa campurejo, kecamatan sambit, campurejo, ponorogo, desa campurejo raharjo";
    }elseif ($meta == "seo_description"){
        return "Desa Campurejo Kecamatan Sambit Kabupaten Ponorogo Provinsi Jawa Timur - Kode POS 63474";
    }
}