<?php


$app->get('/get_data2/provinsi', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_provinsi.*")
        ->from("m_provinsi");

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});
$app->get('/get_data2/kabupaten', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_kabupaten.*, m_provinsi.provinsi")
        ->from("m_kabupaten")
        ->leftJoin("m_provinsi","m_provinsi.id = m_kabupaten.provinsi_id");
    if (isset($params['provinsi_id']) && !empty($params['provinsi_id'])) {
        $db->andWhere("provinsi_id", "=", $params['provinsi_id']);
    }

    if (isset($params['kabupaten']) && !empty($params['kabupaten'])) {
        $db->andWhere("m_kabupaten.kabupaten", "like", $params['kabupaten']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data2/kecamatan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_kecamatan.*, m_kabupaten.kabupaten")
        ->from("m_kecamatan")
        ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id");

    if ($params['kabupaten_id']) {
        $db->andWhere("kabupaten_id", "=", $params['kabupaten_id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data2/kecamatanWithKabupaten', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
        ->from("m_kecamatan")
        ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id");

    if (!empty($params['kecamatan'])) {
        $db->andWhere("m_kecamatan.kecamatan", "like", $params['kecamatan']);
    }

    if (!empty($params['kecamatan_id'])) {
        $db->andWhere("m_kecamatan.id", "=", $params['kecamatan_id']);
    }

    if (!empty($params['kabupaten_id'])) {
        $db->andWhere("m_kecamatan.kabupaten_id", "=", $params['kabupaten_id']);
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data2/desa', function ($request, $response) {
    $params = $request->getParams();
//    print_r($params);die();
    $db = $this->db;

    $db->select("m_desa.*, m_kecamatan.kecamatan")
        ->from("m_desa")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id");

    if (!empty($params['kecamatan_id'])) {
        $db->andWhere("kecamatan_id", "=", $params['kecamatan_id']);
    }

    if (!empty($params['desa'])) {
        $db->andWhere("m_desa.desa", "like", $params['desa']);
    }

    if (!empty($params['id'])) {
        $db->andWhere("m_desa.id", "=", $params['id']);
    }

    if (isset($params['desa_id']) && !empty($params['desa_id']) && $_SESSION['user']['is_super_admin'] != 1) {
        $db->customWhere("m_desa.id IN (" . $_SESSION['user']['desa_id'] . ")", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});

$app->get('/get_data2/perangkatDesa', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("t_struktur_organisasi.*,m_penduduk.nama,m_jabatan.nama as jabatan")
        ->from("t_struktur_organisasi")
        ->innerJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->innerJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id");

    if (!empty($params['desa_id'])) {
        $db->andWhere("t_struktur_organisasi.desa_id", "=", $params['desa_id']);
    }

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


$app->get('/get_data2/tandaTangan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;


    $db->select("t_struktur_organisasi.*, 
    m_penduduk.nama,
    m_penduduk.alamat as alamat_yang_menandatangan,
    m_jabatan.nama as jabatan,m_jabatan.is_kepala_desa")
        ->from("t_struktur_organisasi")
        ->leftJoin("m_penduduk", "m_penduduk.id = t_struktur_organisasi.m_penduduk_id")
        ->leftJoin("m_jabatan", "m_jabatan.id = t_struktur_organisasi.m_jabatan_id")
        ->where("m_jabatan.yang_menandatangani_id", "=", 1)
        ->andWhere("t_struktur_organisasi.is_deleted", "=", 0);

    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("t_struktur_organisasi.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    $models = $db->findAll();

    return successResponse($response, ["list" => $models]);
});


?>