<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua hak akses
 */
$app->get("/appakses/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_roles.*,
    m_desa.desa as nama_desa,
    m_kecamatan.kecamatan,
    m_kabupaten.kabupaten
    ")
        ->from("m_roles")
        ->leftJoin("m_desa", "m_desa.id = m_roles.desa_id")
        ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
        ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id");

    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "kabupaten") {
                $db->where("m_kabupaten.kabupaten", "LIKE", $val);
            } elseif ($key == "kecamatan") {
                $db->where("m_kecamatan.kecamatan", "LIKE", $val);
            } elseif ($key == "desa") {
                $db->where("m_desa.desa", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set Sort
     */
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false") {
                $sort .= " ASC";
            } else {
                $sort .= " DESC";
            }
        }
        $db->orderBy($sort);
    }

    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }


    if (!empty($_SESSION['user']['desa_active']['m_desa_id']) && $_SESSION['user']['is_super_admin'] != 1) {
        $db->customWhere("m_roles.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }


    $models = $db->findAll();
    $totalItem = $db->count();
    foreach ($models as $val) {
        $val->akses = json_decode($val->akses);
        $val->kabupaten = $db->select("m_kabupaten.*, m_provinsi.provinsi")
            ->from("m_kabupaten")
            ->leftJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_kabupaten.id", "=", $val->kabupaten_id)
            ->find();

        $val->kecamatan = $db->select("m_kecamatan.*, m_kabupaten.kabupaten")
            ->from("m_kecamatan")
            ->leftJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_kecamatan.id", "=", $val->kecamatan_id)
            ->find();

        $val->desa = $db->select("m_desa.*, m_kecamatan.kecamatan")
            ->from("m_desa")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
            ->where("m_desa.id", "=", $val->desa_id)
            ->find();

    }
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save hak akses
 */
$app->post("/appakses/save", function ($request, $response) {
    $data = $request->getParams();

//    print_die($data);

    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data['kabupaten']) && !empty($data['kabupaten'])) {
                $data['kabupaten_id'] = $data['kabupaten']['id'];
            }

            if (isset($data['kecamatan']) && !empty($data['kecamatan'])) {
                $data['kecamatan_id'] = $data['kecamatan']['id'];
            }

            if (isset($data['desa']) && !empty($data['desa'])) {
                $data['desa_id'] = $data['desa']['id'];
            }

            $data["akses"] = json_encode($data["akses"]);

            if (isset($data["id"])) {
                $model = $db->update("m_roles", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_roles", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Save status akses
 */
$app->post("/appakses/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
//            $data["akses"] = json_encode($data["akses"]);
            $model = $db->delete("m_roles", ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * save status user
 */
$app->post("/appakses/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    unset($data["password"]);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_roles", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});