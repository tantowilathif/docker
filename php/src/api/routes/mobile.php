<?php


/**
 * Home
 */
$app->get('/mobile/home', function ($request, $response) {
    $db = $this->db;
    $param = $request->getParams();

    $param['m_desa_id'] = isset($param['m_desa_id']) && !empty($param['m_desa_id']) ? $param['m_desa_id'] : 0;

    $content = $db->select("m_gallery.file AS gambar")
        ->from("m_gallery")
        ->where("m_desa_id","=", $param['m_desa_id'])
        ->limit(3)
        ->findAll();

//    $jumlahpend = $db->select("
//    COUNT(m_penduduk.id) AS totalAll")
//        ->from("m_penduduk")
//        ->find();
//
//    $jumlahLk = $db->select("
//    COUNT(m_penduduk.id) AS laki")
//        ->from("m_penduduk")
//        ->where("jenis_kelamin", "=", "LK")
//        ->find();
//
//    $jumlahPr = $db->select("
//    COUNT(m_penduduk.id) AS pr")
//        ->from("m_penduduk")
//        ->where("jenis_kelamin", "=", "PR")
//        ->find();

    return successResponse($response, [
        'jadwal' => getJadwal(),
        'menu' => "home",
        'galeri' => $content,
        'penduduk' => 0,
        'cewek' => 0,
        'cowok' => 0,
        'artikel' => getAllArtikel(),
    ]);
});

/**
 * Pengajuan Surat
 */
$app->get('/pengajuan', function ($request, $response) {
    $db = $this->db;

//    echo json_encode(getAllArtikel());    exit();

    return $this->view->render($response, 'pengajuan.twig', [
        'menu' => "home",
        'jadwal' => getJadwal(),
    ]);
});

/**
 * Acara
 */
$app->get('/acara', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'acara.twig', [
        'menu' => "produk",
        'jadwal' => getJadwal(),
    ]);
});

/**
 * Master Plan
 */
$app->get('/product/master-plan.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/master-plan.twig', [
        'menu' => "master-plan",
        'sosmed' => getSettingweb(),
        'master_plan' => getArtikelMasterPlan(),
        'header' => "Master Plan",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_produk' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});
/**
 * Fasilitas
 */
$app->get('/product/fasilitas.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/fasilitas.twig', [
        'menu' => "master-plan",
        'sosmed' => getSettingweb(),
        'fasilitas' => getArtikelFasilitas(),
        'header' => "Fasilitas & Keunggulan",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_produk' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});

/**
 * Galley
 */
$app->get('/mobile/gallery', function ($request, $response) {
    $db = $this->db;
    $param['m_desa_id'] = isset($param['m_desa_id']) && !empty($param['m_desa_id']) ? $param['m_desa_id'] : 0;

    $db->select("*")
        ->from("m_gallery")
        ->where("m_desa_id","=", $param['m_desa_id'])
        ->where("is_primary", "=", 1);
    $db->groupBy("kategori_id");
    $gallery = $db->findAll();

    $db->select("count(id) as jumlah, kategori_id")
        ->from("m_gallery")
        ->where("m_desa_id","=", $param['m_desa_id']);

    $jumlah = $db->groupBy("kategori_id")->findAll();
    $arr = [];
    foreach ($jumlah as $key => $value) {
        $arr[$value->kategori_id] = $value->jumlah;
    }

    $data = [];
    foreach ($gallery as $key => $value) {
        $value->jumlah = isset($arr[$value->kategori_id]) ? $arr[$value->kategori_id] : 0;
        $data[$key] = (array)$value;
    }

    return successResponse($response, [
        'jadwal' => getJadwal(),
        'gallery' => $data,
    ]);
});

$app->get('/mobile/detail_gallery', function ($request, $response) {
    $param = $request->getParams();
    $db = $this->db;
    $param['m_desa_id'] = isset($param['m_desa_id']) && !empty($param['m_desa_id']) ? $param['m_desa_id'] : 0;

    $db->select("*")
        ->from("m_gallery")
        ->where("m_desa_id","=", $param['m_desa_id']);

    if (isset($param['kategori']) && !empty($param)) {
        $db->where("kategori_id", "=", $param['kategori']);
    }
    $gallery = $db->findAll();

    return successResponse($response, [
        'jadwal' => getJadwal(),
        'detgallery' => $gallery,
    ]);
});

//jajal cari artikel
$app->get('/mobile/artikel/cari/{search}', function ($request, $response) {
    $params = $request->getAttribute("search");
    $db = $this->db;

    $param['m_desa_id'] = isset($param['m_desa_id']) && !empty($param['m_desa_id']) ? $param['m_desa_id'] : 0;


    $cari = $db->select("m_artikel.title AS judul_artikel, m_kategori_artikel.nama AS kategori_artikel")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.kategori_artikel_id=m_kategori_artikel.id")
        ->where("m_artikel.title", "like", $params)
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->findAll();

    return successResponse($response, $cari);
});


/**
 * Berita Acara
 */
$app->get('/mobile/artikel', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $params['m_desa_id'] = isset($params['m_desa_id']) && !empty($params['m_desa_id']) ? $params['m_desa_id'] : 0;

    if (!empty($params['page'])) {
        $page = ($params['page'] > 0) ? $params['page'] : 1;
    } else {
        $page = 1;
    }

    $limit = 1;
    $skip = ($page - 1) * $limit;

    $db->select("m_artikel.*,
    m_artikel.title as judul,
    m_kategori_artikel.nama AS kategori,
    m_user.nama AS publisher")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.kategori_artikel_id=m_kategori_artikel.id")
        ->join("left join", "m_user", "m_user.id = m_artikel.created_by")
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->limit($limit)
        ->offset($skip);

    $model = $db->orderBy("id DESC")->findAll();
    $count = $db->count();
//        print_die($count);

    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;
        $model[$key]['gambar_thumbfun'] = gambar_pertama($val->content);
    }

    $first = 1;
    $last = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));

    $kategori = $db->select("m_kategori_artikel.nama AS kategori")
        ->from("m_kategori_artikel")
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->groupby("m_kategori_artikel.id")
        ->findAll();

    return successResponse($response, [
        "artikel" => $model,
        'totalItems' => $count,
        "category" => $kategori,
        'artikel_terbaru' => getAllArtikel(),
        'jadwal' => getJadwal(),
        'next' => $page + 1,
        'prev' => $page - 1,
        'first' => $first,
        'last' => $last,
        'thispage' => $page,
        'pagination' => [
            'needed' => $count > $limit,
            'count' => $count,
            'active' => $page,
            'lastpage' => $last,
            'limit' => $limit,
        ],
    ]);
});


/**
 * Berita Artikel Detail
 */
$app->get('/mobile/artikel/{alias}', function ($request, $response) {
    $params = $request->getAttribute("alias");
    $db = $this->db;
    $params['m_desa_id'] = isset($params['m_desa_id']) && !empty($params['m_desa_id']) ? $params['m_desa_id'] : 0;

    $content = $db->select("m_artikel.*,
    m_artikel.content as isi_artikel,
    m_artikel.title as judul,
    m_user.nama as penulis, 
    m_kategori_artikel.nama AS kategori")
        ->from("m_artikel")
        ->join("left join", "m_user", "m_user.id = m_artikel.created_by")
        ->join("left join", "m_kategori_artikel", "m_artikel.kategori_artikel_id=m_kategori_artikel.id")
        ->where("publish", "=", 1)
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->andWhere("m_artikel.alias", '=', $params)
        ->orderBy("m_artikel.id DESC")
        ->find();

    if (!empty($content)) {
        $content->judultitle = $content->title;
        $content->jam = date('H:i', $content->created_at);
        $content->gambar_thumb = gambar_pertama($content->content);
        $content->created_at = date('d F Y', $content->created_at);
    }

    $kategori = $db->select("m_kategori_artikel.nama AS kategori")
        ->from("m_kategori_artikel")
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->groupby("m_kategori_artikel.id")
        ->findAll();

//    echo json_encode(getNextArtikel($content->id));exit();
    return successResponse($response, [
        'menu' => "portofolio",
        'artikel' => $content,
        'category' => $kategori,
        'artikel_terbaru' => getAllArtikel(),
        'jadwal' => getJadwal(),
    ]);
});

/**
 * Berita
 */
$app->get('/mobile/informasi', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $params['m_desa_id'] = isset($params['m_desa_id']) && !empty($params['m_desa_id']) ? $params['m_desa_id'] : 0;

    if (!empty($params['page'])) {
        $page = ($params['page'] > 0) ? $params['page'] : 1;
    } else {
        $page = 1;
    }

    $limit = 1;
    $skip = ($page - 1) * $limit;

    $db->select("m_artikel.*,
    m_artikel.title as judul,
    m_kategori_artikel.nama AS kategori,
    m_user.nama AS publisher")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.kategori_artikel_id=m_kategori_artikel.id")
        ->join("left join", "m_user", "m_user.id = m_artikel.created_by")
        ->where("m_kategori_artikel.id", "=", 2)
        ->where("m_artikel.m_desa_id", "=", $params['m_desa_id'])
        ->limit($limit)
        ->offset($skip);

    $model = $db->orderBy("id DESC")->findAll();
    $count = $db->count();
//        print_die($count);

    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;
        $model[$key]['gambar_thumbfun'] = gambar_pertama($val->content);
    }

    $first = 1;
    $last = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));

    $kategori = $db->select("m_kategori_artikel.nama AS kategori")
        ->from("m_kategori_artikel")
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->groupby("m_kategori_artikel.id")
        ->findAll();

    return successResponse($response, [
        "artikel" => $model,
        'totalItems' => $count,
        "category" => $kategori,
        'artikel_terbaru' => getAllArtikel(),
        'jadwal' => getJadwal(),
        'next' => $page + 1,
        'prev' => $page - 1,
        'first' => $first,
        'last' => $last,
        'thispage' => $page,
        'pagination' => [
            'needed' => $count > $limit,
            'count' => $count,
            'active' => $page,
            'lastpage' => $last,
            'limit' => $limit,
        ],
    ]);
});

/**
 * Berita
 */
$app->get('/mobile/berita', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $params['m_desa_id'] = isset($params['m_desa_id']) && !empty($params['m_desa_id']) ? $params['m_desa_id'] : 0;

    if (!empty($params['page'])) {
        $page = ($params['page'] > 0) ? $params['page'] : 1;
    } else {
        $page = 1;
    }

    $limit = 1;
    $skip = ($page - 1) * $limit;

    $db->select("m_artikel.*,
    m_artikel.title as judul,
    m_kategori_artikel.nama AS kategori,
    m_user.nama AS publisher")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.kategori_artikel_id=m_kategori_artikel.id")
        ->join("left join", "m_user", "m_user.id = m_artikel.created_by")
        ->where("m_kategori_artikel.id", "=", 1)
        ->where("m_artikel.m_desa_id", "=", $params['m_desa_id'])
        ->limit($limit)
        ->offset($skip);

    $model = $db->orderBy("id DESC")->findAll();
    $count = $db->count();
//        print_die($count);

    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;
        $model[$key]['gambar_thumbfun'] = gambar_pertama($val->content);
    }

    $first = 1;
    $last = (ceil($count / $limit) == 0 ? 1 : ceil($count / $limit));

    $kategori = $db->select("m_kategori_artikel.nama AS kategori")
        ->from("m_kategori_artikel")
        ->where("m_desa_id","=", $params['m_desa_id'])
        ->groupby("m_kategori_artikel.id")
        ->findAll();

    return successResponse($response,  [
        "artikel" => $model,
        'totalItems' => $count,
        "category" => $kategori,
        'artikel_terbaru' => getAllArtikel(),
        'jadwal' => getJadwal(),
        'next' => $page + 1,
        'prev' => $page - 1,
        'first' => $first,
        'last' => $last,
        'thispage' => $page,
        'pagination' => [
            'needed' => $count > $limit,
            'count' => $count,
            'active' => $page,
            'lastpage' => $last,
            'limit' => $limit,
        ],
    ]);
});


$app->get('/contact', function ($request, $response) {
    $db = $this->db;
    $submit = $_GET;
//    print_die($submit);

//    $db->insert("m_komentar", $submit);

    return $this->view->render($response, 'contact.twig', [
        'menu' => "contact",
        'jadwal' => getJadwal(),
//        'sosmed' => getSettingweb(),
//        'header' => "Contact Us",
//        'no_wa' => getnoWa(),
//        'judul' => "Stay With Us",
//        'foto_header' => "contact-us2.jpg",
//        'is_kontak' => true,
//        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
//        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
//        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",

    ]);
});

/**
 * History
 */
$app->get('/history', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'history.twig', [
        'jadwal' => getJadwal(),
//        'menu' => "kontak-kami",
//        'sosmed' => getSettingweb(),
//        'header' => "Contact Us",
//        'no_wa' => getnoWa(),
//        'judul' => "Stay With Us",
//        'foto_header' => "contact-us2.jpg",
//        'is_kontak' => true,
//        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
//        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
//        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",

    ]);
});

/**
 * INFO!!
 */


/**
 * Keluhan Pelanggan
 */
$app->get('/info/keluhan-pelanggan.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/keluhan-pelanggan.twig', [
        'sosmed' => getSettingweb(),
        'header' => "Keluhan Pelanggan",
        'no_wa' => getnoWa(),
        'judul' => "Taman Dhika Sidoarjo",
        'foto_header' => "slide2.jpg",
        'is_info' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",

    ]);
});


/**
 * Legal Information
 */
$app->get('/info/legal-information.html', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'content/legal-information.twig', [
        'sosmed' => getSettingweb(),
        'artikel' => getAllLegalInformation(),
        'header' => "Legal Information",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_info' => true,
        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",


    ]);
});
/**
 * Legal Information Detail
 */
$app->get('/legal-information/{alias}.html', function ($request, $response) {
    $params = $request->getAttribute("alias");

    $db = $this->db;
    $content = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->andWhere("artikel.alias", '=', $params)
        ->orderBy("artikel.id DESC")
        ->findAll();
    foreach ($content as $key => $value) {
        $value->judultitle = $value->judul;
        $value->jam = date('H:i', $value->jam);
        $value->gambar_thumb = gambar_pertama($value->isi_content);
        $value->created_at = date('d F Y', $value->created_at);
        $value->tgl_kontrak = date('d F Y', strtotime($value->tgl_kontrak));
    }


    return $this->view->render($response, 'content/legal-information-detail.twig', [
        'menu' => "portofolio",
        'sosmed' => getSettingweb(),
        'artikel' => $content,
        'next_artikel' => getNextLegalInformation($value->id),
        'header' => "Berita & Acara ",
        'judul' => "Taman Dhika Sidoarjo",
        'no_wa' => getnoWa(),
        'foto_header' => "slide2.jpg",
        'is_berita' => true,
        'seo_title' => " $value->judul",
        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
        'seo_description' => " $value->deskripsi",

    ]);
});


/**
 * About
 */
$app->get('/about', function ($request, $response) {
    $db = $this->db;
    return $this->view->render($response, 'about.twig', [
        'menu' => "tentang-kami",
        'jadwal' => getJadwal(),

    ]);
});
/**
 * Produk Detail
 */
//$app->get('/{nama}.html', function ($request, $response) {
//    $params = $request->getAttribute("nama");
//    $db = $this->db;
//
//
//    return $this->view->render($response, 'content/product-detail.twig', [
//        'sosmed' => getSettingweb(),
//        'productDetail' => getProductDetail($params),
//        'judul' => getProductDetailNama($params),
//        'no_wa' => getnoWa(),
//        'header' => "Taman Dhika Sidoarjo",
//        'foto_header' => "slide2.jpg",
//        'is_produk' => true,
//        'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
//        'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
//        'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
//
//    ]);
//});


/**
 * Send Whatsapp
 */
$app->post('/kirim-wa', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $model = $db->select("*")
        ->from("setting")
        ->find();
    $nama = $params['nama'];
    $pesan = $params['pesan'];
    $email = $params['email'];


    $to = $model->email;
    $subject = $nama;
    $txt = $pesan;
    $txt = " Email dari " . $email . " dengan Nama : " . $nama . " Pesan " . $pesan;

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = "noreplyinfosystems@gmail.com";
    $mail->Password = "bismillah2018";
    /*$mail->Username = $getEmail->email_smtp;
    $mail->Password = $getEmail->password_smtp;*/
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('info@landa.co.id', "Taman Dhika Sidoarjo");
    $mail->addAddress($to, "$to");
    // foreach ($email_penerima as $key => $value) {
    // $mail->addAddress($value);
    // }

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body = $txt;

    if (!$mail->send()) {
        return $this->view->render($response, 'content/kontak-kami.twig', [
            'menu' => "kontak-kami",
            'sosmed' => getSettingweb(),
            'judul' => "Stay With Us",
            'header' => "Kontak Kami",
            'balasan' => 'Gagal',
            'foto_header' => "contact-us2.jpg",
            'is_kontak' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "gagal"


        ]);
    } else {
        return $this->view->render($response, 'content/kontak-kami.twig', [
            'menu' => "kontak-kami",
            'sosmed' => getSettingweb(),
            'header' => "Kontak Kami",
            'judul' => "Stay With Us",
            'balasan' => 'Berhasil',
            'foto_header' => "contact-us2.jpg",
            'is_kontak' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "berhasil"

        ]);
    }
    /*} else {
        return [
            'status' => false,
            'error' => 'Email SMTP Belum di setting',
        ];
    }*/

});


/**
 * Send Keluhan
 */
$app->post('/kirim-keluhan', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $model = $db->select("*")
        ->from("setting")
        ->find();


    $keluhan = $db->insert("m_keluhan", $params);


//    echo json_encode($keluhan);
//    die();
    $nama = $params['nama'];
    $judul = $params['subject'];
    $email = $params['email'];
    $keluhan_detail = $params['description'];
    $blok = $params['blok'];
    $unit = $params['unit'];


    $to = $model->email;
    $subject = "Keluhan Pelanggan";
    $txt = " Email dari " . $email . " dengan Nama : " . $nama . " ,judul Keluhan  : " . $judul . " ,Keluhan  : " . $keluhan_detail . " ,di Blok : " . $blok . " ,Unit : " . $unit;

    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;

    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = "noreplyinfosystems@gmail.com";
    $mail->Password = "bismillah2018";
    /*$mail->Username = $getEmail->email_smtp;
    $mail->Password = $getEmail->password_smtp;*/
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->setFrom('info@landa.co.id', "Taman Dhika Sidoarjo");
    $mail->addAddress($to, "$to");
    // foreach ($email_penerima as $key => $value) {
    // $mail->addAddress($value);
    // }

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body = $txt;

    if (!$mail->send()) {

        return $this->view->render($response, 'content/keluhan-pelanggan.twig', [
            'sosmed' => getSettingweb(),
            'header' => "Keluhan Pelanggan",
            'no_wa' => getnoWa(),
            'judul' => "Taman Dhika Sidoarjo",
            'foto_header' => "slide2.jpg",
            'is_info' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "gagal"

        ]);
    } else {
        return $this->view->render($response, 'content/keluhan-pelanggan.twig', [
            'sosmed' => getSettingweb(),
            'header' => "Keluhan Pelanggan",
            'no_wa' => getnoWa(),
            'judul' => "Taman Dhika Sidoarjo",
            'foto_header' => "slide2.jpg",
            'is_info' => true,
            'seo_title' => "Taman Dhika Sidoarjo - Hunian Premium Kota Sidoarjo ",
            'seo_keywords' => "Perumahan Kota Sidoarjo, Hunian Premium Kota Sidoarjo,Perumahan Dekat Tol Sidoarjo, Perumahan Dekat Bandara Juanda ",
            'seo_description' => "Perumahan Taman Dhika Sidoarjo Kota Dibangun oleh Developer BUMN yakni Adhi Persada Properti yang telah dipercaya membangun kawasan yang nyaman ditinggali dan bernilai tinggi di berbagai kota besar di Indonesia",
            'pesan' => "berhasil"

        ]);
    }
    /*} else {
        return [
            'status' => false,
            'error' => 'Email SMTP Belum di setting',
        ];
    }*/

});