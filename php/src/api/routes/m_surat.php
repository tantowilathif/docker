<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
        "file" => "required"
    );
//    GUMP::set_field_name("no_urut", "No Urut");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_surat/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_surat")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil semua list user
 */
$app->get("/m_surat/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_surat.*")
        ->from("m_surat");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_surat.nama", "LIKE", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    $totalItem = $db->count();

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_surat/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data);
    if ($validasi === true) {

        if (!empty($data['kop']['base64'])) {
            $folder = '../img/kop';
            if (!is_dir($folder)) {
                mkdir($folder, 0777);
            }
            $simpan_kop = base64ToFile($data['kop'], $folder);
            $temp = explode(".", $simpan_kop["fileName"]);
            $newfilename = 'kop-' . round(microtime(true)) . '.' . end($temp);
            rename($simpan_kop['filePath'], $folder . '/' . $newfilename);
            $data['kop'] = $newfilename;
        }
        if (!empty($data['footer']['base64'])) {
            $folder = '../img/footer';
            if (!is_dir($folder)) {
                mkdir($folder, 0777);
            }
            $simpan_footer = base64ToFile($data['footer'], $folder);
            $temp = explode(".", $simpan_footer["fileName"]);
            $newfilename = 'footer-' . round(microtime(true)) . '.' . end($temp);
            rename($simpan_footer['filePath'], $folder . '/' . $newfilename);
            $data['footer'] = $newfilename;
        }

        if (isset($data["id"])) {
            $model = $db->update("m_surat", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("m_surat", $data);
        }

        if ($model) {
            return successResponse($response, $model);
        }
    }
    return unprocessResponse($response, $validasi);

});
/**
 * save status user
 */
$app->post("/m_surat/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_surat", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
