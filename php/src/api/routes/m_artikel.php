<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array()) {
    $validasi = array(
        'title' => 'required',
        'kategori_artikel' => 'required',
        'content' => 'required',
    );

    GUMP::set_field_name("title", "Judul");
    GUMP::set_field_name("kategori_artikel", "Kategori Artikel");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * get user list
 */
$app->get('/m_artikel/index', function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;

    $db->select("m_artikel.*, m_kategori_artikel.nama as kategori, m_artikel.tgl_publish as tanggal")
            ->from('m_artikel')
            ->join('left join', 'm_kategori_artikel', 'm_artikel.kategori_artikel_id = m_kategori_artikel.id')
            ->orderBy('m_artikel.id desc');

    /** set parameter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'title') {
                $db->where('m_artikel.title', 'LIKE', $val);
            } else if ($key == 'is_deleted') {
                $db->where('m_artikel.is_deleted', '=', $val);
            } elseif ($key == 'kategori_artikel_id') {
                $db->andWhere('m_artikel.m_artikel_kategori_id', '=', $val);
            } else {
                $db->where($key, 'LIKE', $val);
            }
        }
    }

    /**
     * FILTER BERDASARKAN DESA YANG AKTIF
     */
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_artikel.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    /** Set limit */
    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    /** Set offset */
    if (isset($params['offset']) && !empty($params['offset'])) {
        $db->offset($params['offset']);
    }

    /** Set sorting */
    if (isset($params['sort']) && !empty($params['sort'])) {
        $db->orderBy($params['sort']);
    }

    $models = $db->findAll();
    $totalItem = $db->count();
//     print_r($models);exit;
    $db->select("*")
            ->from("m_kategori_artikel");
    $det = $db->findAll();

    foreach ($models as $key => $value) {
        $value->tgl_publish = date("d M Y", $value->tgl_publish);
        $models[$key] = (array) $value;
        $models[$key]['publish'] = (string) $models[$key]['publish'];

        foreach ($det as $keys => $vals) {
            if ($vals->id == $value->kategori_artikel_id) {
                $models[$key]['kategori_artikel']['id'] = $vals->id;
                $models[$key]['kategori_artikel']['nama'] = $vals->nama;
            }
        }
    }

//    print_die($models);

    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * create
 */
$app->post('/m_artikel/create', function ($request, $response) {
    $db = $this->db;
    $data = json_decode(file_get_contents("php://input"), true);
//    print_die($data);

    $validasi = validasi($data);

    if ($validasi === true) {
        try {
            $data['is_deleted'] = 0;
            $data['kategori_artikel_id'] = $data['kategori_artikel']['id'];
            $data['alias'] = urlParsing($data['title']);
            $data['tgl_publish'] = strtotime($data['tanggal']);
            $model = $db->insert("m_artikel", $data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * update user
 */
$app->post('/m_artikel/update', function ($request, $response) {
    $data = json_decode(file_get_contents("php://input"), true);
    $db = $this->db;


    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['alias'] = isset($data['title']) ? urlParsing($data['title']) : "";
            $data['kategori_artikel_id'] = $data['kategori_artikel']['id'];
            $data['alias'] = urlParsing($data['title']);
            $data['tgl_publish'] = strtotime($data['tanggal']);
            $model = $db->update("m_artikel", $data, array('id' => $data['id']));

            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post('/m_artikel/trash', function ($request, $response) {
    $data = json_decode(file_get_contents("php://input"), true);
    $db = $this->db;
    $datas['is_deleted'] = $data['is_deleted'];
    try {
        $model = $db->update("m_artikel", $datas, array('id' => $data['id']));
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ['data gagal disimpan']);
    }
});

/**
 * delete user
 */
//$app->delete('/m_artikel/delete/{id}', function ($request, $response) {
//    $sql = $this->db;
//    $delete = $sql->delete('m_artikel', array('id' => $request->getAttribute('id')));
//    echo json_encode(array('status' => 1));
//});

$app->post("/m_artikel/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("m_artikel", ["id" => $data["id"]]);

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});


