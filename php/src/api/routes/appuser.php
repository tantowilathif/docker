<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama" => "required",
        "username" => "required",
        "m_roles_id" => "required",
    );
    GUMP::set_field_name("m_roles_id", "Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/appuser/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_user")
        ->where("is_deleted", "=", 0);
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil data user untuk update profil
 */
$app->get("/appuser/view", function ($request, $response) {
    $db = $this->db;
        $data = $db->find("select * from m_user where id = '" . $_SESSION["user"]["id"] . "'");
        unset($data->password);
        return successResponse($response, $data);
});
/**
 * Ambil semua list user
 */
$app->get("/appuser/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_user.*, m_roles.is_super_admin, m_roles.nama as hakakses,
    m_kecamatan.kecamatan,
    m_kecamatan.kabupaten_id,
    m_desa.desa
    ")
        ->from("m_user")
        ->join("left join", "m_roles", "m_user.m_roles_id = m_roles.id")
        ->join("left join", "m_kecamatan", "m_kecamatan.id = m_roles.kecamatan_id")
        ->join("left join", "m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
        ->join("left join", "m_desa", "m_desa.id = m_roles.desa_id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_user.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_user.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */

    if (!empty($_SESSION['user']['desa_active']['m_desa_id']) && $_SESSION['user']['is_super_admin'] != 1) {
        $db->leftJoin("m_roles_desa", "m_roles_desa.m_user_id = m_user.id");
        $db->customWhere("m_roles_desa.m_desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
        $db->where("m_roles.is_super_admin", "!=", 1);
    }

    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $totalItem = $db->count();
    $models = $db->orderBy("m_user.id DESC")->findAll();

    $arr = [];
    foreach ($models as $key => $val) {
        $val->m_roles_id = (string)$val->m_roles_id;
        $val->kabupaten = $db->select("m_kabupaten.*,m_provinsi.provinsi")
            ->from("m_kabupaten")
            ->innerJoin("m_provinsi", "m_provinsi.id = m_kabupaten.provinsi_id")
            ->where("m_kabupaten.id", "=", $val->kabupaten_id)
            ->find();
        $val->kecamatan = $db->select("m_kecamatan.*,m_kabupaten.kabupaten")
            ->from("m_kecamatan")
            ->innerJoin("m_kabupaten", "m_kabupaten.id = m_kecamatan.kabupaten_id")
            ->where("m_kecamatan.id", "=", $val->kecamatan_id)
            ->find();

        $val->m_roles_desa = $db->select("m_roles_desa.m_desa_id as id, m_roles_desa.m_desa_id, m_roles_desa.m_user_id, m_desa.desa")
            ->from("m_roles_desa")
            ->leftJoin("m_desa", "m_desa.id = m_roles_desa.m_desa_id")
            ->where("m_roles_desa.m_user_id", "=", $val->id)
            ->findAll();

        $val->arrDesa = [];
        if (!empty($val->m_roles_desa)) {
            foreach ($val->m_roles_desa as $k => $v) {
                $val->arrDesa[] = $v->m_desa_id;
            }
        }

        if (empty($_SESSION['user']['is_super_admin'])) {
            if (in_array($_SESSION['user']['desa_active']['m_desa_id'], $val->arrDesa)) {
                $arr[$key] = $val;
            }
        } else {
            $arr[$key] = $val;
        }
    }


    return successResponse($response, ["list" => $arr, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/appuser/save", function ($request, $response) {
    $data = $request->getParams();
//print_die($data);
    $db = $this->db;
    if (isset($data["id"])) {
        /**
         * Update user
         */
        if (!empty($data["password"])) {
            $data["password"] = sha1($data["password"]);
        } else {
            unset($data["password"]);
        }
        $validasi = validasi($data);
    } else {
        /**
         * Buat user baru
         */
        $data["password"] = isset($data["password"]) ? sha1($data["password"]) : "";
        $validasi = validasi($data, ["password" => "required"]);
    }

    if ($validasi === true) {
        try {
            $data["kabupaten_id"] = (!empty($data['kabupaten']['id'])) ? $data['kabupaten']['id'] : null;
            $data["kecamatan_id"] = (!empty($data['kecamatan']['id'])) ? $data['kecamatan']['id'] : null;

            if (isset($data["id"])) {
                $model = $db->update("m_user", $data, ["id" => $data["id"]]);
                if (isset($data['m_roles_desa']) && !empty($data['m_roles_desa'])) {
                    $db->delete("m_roles_desa", ["m_user_id" => $data["id"]]);
                }

            } else {
                $model = $db->insert("m_user", $data);
            }

            if (!empty($data["m_roles_desa"]) && $_SESSION['user']['is_super_admin'] == 1) {
//                echo json_encode($data['m_roles_desa']);exit();
                foreach ($data['m_roles_desa'] as $key => $value) {
                    $detail["m_user_id"] = isset($model->id) ? $model->id : '';
                    $detail["m_desa_id"] = $value['id'];
//                    echo $value['id'];
                    $create = $db->insert("m_roles_desa", $detail);
                }
            } elseif (isset($data["desa"]) && !empty($data["desa"])) {
                $detail["m_user_id"] = isset($model->id) ? $model->id : '';
                $detail["m_desa_id"] = $data["desa"]["id"];
                $create = $db->insert("m_roles_desa", $detail);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/appuser/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    unset($data["password"]);
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->update("m_user", $data, ["id" => $data["id"]]);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});