<?php

/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "email" => "required",
//        "no_tlp" => "required",
//        "alamat" => "required"
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua m fasilitas
 */
$app->get("/m_setting/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_setting.*")
        ->from("m_setting");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $models = $db->findAll();

    foreach ($models as $key => $val) {
        $val->desa = $db->select("m_desa.*, m_kecamatan.kecamatan")
            ->from("m_desa")
            ->leftJoin("m_kecamatan", "m_kecamatan.id = m_desa.kecamatan_id")
            ->where("m_desa.id", "=", $_SESSION['user']['desa_active']['m_desa_id'])
            ->find();

        $val->kategori_kegiatan = $db->select("*")->from("m_kategori_artikel")->where("id","=",$val->kategori_kegiatan_id)->find();
        $val->kategori_berita = $db->select("*")->from("m_kategori_artikel")->where("id","=",$val->kategori_berita_id)->find();
    }

    return successResponse($response, ["list" => $models]);
});

$app->post("/m_setting/save", function ($request, $response) {
    $data = $request->getParams();
//    print_die($data);
    $db = $this->db;
    $validasi = validasi($data);
//    $folder = './file/kop_surat/';
//    if (!is_dir($folder)) {
//        mkdir($folder, 0777);
//    }

    if ($validasi === true) {
        try {

            $cek = $db->select("id,desa_id")
                ->from("m_setting")
                ->where("desa_id", "=", @$_SESSION['user']['desa_active']['m_desa_id'])
                ->find();

//            print_die($cek);

            $data["kategori_kegiatan_id"] = (isset($data["kategori_kegiatan"]["id"]))?$data["kategori_kegiatan"]["id"]:null;
            $data["kategori_berita_id"] = (isset($data["kategori_berita"]["id"]))?$data["kategori_berita"]["id"]:null;

            if ($cek != false) {
//                $data['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;

//                if (isset($data['kop_surat']['base64'])) {
//                    $simpan_foto = base64ToFile($data['kop_surat'], $folder);
//                    $newfilename = $simpan_foto['fileName'];
//                    $data['kop_surat'] = $newfilename;
//                    $data['path_kop'] = substr($folder, 2);
//                }
//
//                if (isset($data['logo']['base64'])) {
//                    $simpan_foto = base64ToFile($data['logo'], $folder);
//                    $newfilename = $simpan_foto['fileName'];
//                    $data['logo'] = $newfilename;
//                    $data['path_logo'] = substr($folder, 2);
//                }

                $model = $db->update("m_setting", $data, ["desa_id" => $data["desa_id"]]);
            } else {
//                $data['desa_id'] = isset($data['desa']) ? $data['desa']['id'] : null;
//                if (isset($data['kop_surat']['base64'])) {
//                    $simpan_foto = base64ToFile($data['kop_surat'], $folder);
//                    $newfilename = $simpan_foto['fileName'];
//                    $data['kop_surat'] = $newfilename;
//                    $data['path_kop'] = substr($folder, 2);
//                }
//
//                if (isset($data['logo']['base64'])) {
//                    $simpan_foto = base64ToFile($data['logo'], $folder);
//                    $newfilename = $simpan_foto['fileName'];
//                    $data['logo'] = $newfilename;
//                    $data['path_logo'] = substr($folder, 2);
//                }

//                print_die($data);
                unset($data['id']);
                $model = $db->insert("m_setting", $data);
            }

            $_SESSION['user']['setting_aplikasi'] = $model;
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, [$e]);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->get("/m_setting/getLogo", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_setting.*")
        ->from("m_setting");

    if (isset($_SESSION['user']['desa_active']) && !empty($_SESSION['user']['desa_active'])) {
        $db->where("desa_id", "=", $_SESSION['user']['desa_active']['m_desa_id']);
    }

    $models = $db->find();

    return successResponse($response, ["list" => $models]);
});
