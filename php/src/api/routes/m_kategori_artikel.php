<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
//        "Instagram"       => "required",
//        "Whatsapp"       => "required",
//        "Pesan"       => "required",
        // "username"   => "required",
        // "m_roles_id" => "required",
    );
//    GUMP::set_field_name("Instagram", "Instagram");
//    GUMP::set_field_name("Whatsapp", "Whatsapp");
//    GUMP::set_field_name("Pesan", "Pesan");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil semua user aktif tanpa pagination
 */
$app->get("/m_kategori_artikel/getAll", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_kategori_artikel");
//         ->where("is_deleted", "=", 0);

//    echo json_encode($params);die();
    if (isset($params["nama"]) && !empty($params["nama"])) {
        $db->where("nama", "LIKE", $params["nama"]);
    }
    $models = $db->findAll();
    return successResponse($response, $models);
});
/**
 * Ambil data user untuk update profil
 */
$app->get("/m_kategori_artikel/view", function ($request, $response) {
    $db = $this->db;
    $data = $db->find("select * from m_kategori_artikel where id = '" . $_SESSION["user"]["id"] . "'");
    unset($data->password);
    return successResponse($response, $data);
});
/**
 * Ambil semua list user
 */
$app->get("/m_kategori_artikel/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_kategori_artikel.*")->from("m_kategori_artikel");
//    echo json_encode($db);die();
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == "nama") {
                $db->where("m_kategori_artikel.nama", "LIKE", $val);
            } else if ($key == "is_deleted") {
                $db->where("m_kategori_artikel.is_deleted", "=", $val);
            } else {
                $db->where($key, "LIKE", $val);
            }
        }
    }

    /**
     * FILTER BERDASARKAN DESA YANG AKTIF
     */
    if (!empty($_SESSION['user']['desa_active']['m_desa_id'])) {
        $db->customWhere("m_kategori_artikel.desa_id = " . $_SESSION['user']['desa_active']['m_desa_id'] . "", "AND");
    }

    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    $totalItem = $db->count();

    // foreach ($models as $key => $val) {
    //     $val->m_roles_id = (string) $val->m_roles_id;
    // }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * save user
 */
$app->post("/m_kategori_artikel/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
//     print_r($db);die;
    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            if (isset($data["id"])) {
                $model = $db->update("m_kategori_artikel", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_kategori_artikel", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * save status user
 */
$app->post("/m_kategori_artikel/saveStatus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    unset($data["password"]);
    // $validasi = validasi($data);
    // if ($validasi === true) {
    $cek = $db->select("*")->from("m_setting")->where("desa_id", "=", $_SESSION["user"]["desa_active"]["m_desa_id"])->find();
    if (isset($cek->kategori_berita_id)) {
        if ($cek->kategori_berita_id == $data["id"] || $cek->kategori_kegiatan_id == $data["id"]){
            return unprocessResponse($response, "Data kategori digunakan pada setting aplikasi.");
        }else{
            try {
                $model = $db->update("m_kategori_artikel", $data, ["id" => $data["id"]]);
                return successResponse($response, $model);
            } catch (Exception $e) {
                return unprocessResponse($response, [$e]);
            }
        }
    }

    return unprocessResponse($response, $validasi);
});

$app->post("/m_kategori_artikel/delete", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {

        $model = $db->delete("m_kategori_artikel", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }

    return unprocessResponse($response, $validasi);
});

