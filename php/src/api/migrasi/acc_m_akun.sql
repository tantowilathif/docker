-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 13, 2020 at 06:31 AM
-- Server version: 10.0.38-MariaDB-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `landaid_tenant`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_m_akun`
--

CREATE TABLE `acc_m_akun` (
  `id` int(11) NOT NULL,
  `kode` varchar(45) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tipe` varchar(45) DEFAULT NULL,
  `tipe_arus` varchar(45) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `is_tipe` tinyint(1) DEFAULT '0',
  `is_kas` tinyint(2) NOT NULL DEFAULT '0',
  `is_induk` tinyint(2) DEFAULT NULL,
  `saldo_normal` int(2) NOT NULL DEFAULT '1',
  `tgl_nonaktif` date DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `acc_m_akun`
--

INSERT INTO `acc_m_akun` (`id`, `kode`, `nama`, `tipe`, `tipe_arus`, `parent_id`, `level`, `is_tipe`, `is_kas`, `is_induk`, `saldo_normal`, `tgl_nonaktif`, `is_deleted`) VALUES
(1, '100', 'AKTIVA', 'HARTA', NULL, NULL, 1, 1, 0, 1, 1, NULL, 0),
(2, '100-1', 'AKTIVA LANCAR', 'HARTA', NULL, 1, 2, 1, 0, 0, 1, NULL, 0),
(3, '100-11', 'CASH ON HAND AND BANK', 'HARTA', NULL, 2, 3, 1, 0, 0, 1, NULL, 0),
(4, '100-111', 'CASH ON HAND', 'HARTA', NULL, 3, 4, 1, 0, 0, 1, NULL, 0),
(5, '100-111-01', 'Petty Cash Operational', 'HARTA', NULL, 4, 5, 0, 1, 0, 1, NULL, 0),
(6, '100-112', 'BANK', 'HARTA', NULL, 3, 4, 1, 0, 0, 1, NULL, 0),
(7, '100-112-01', 'Bank BCA PT 6871978899', 'HARTA', NULL, 6, 5, 0, 1, 0, 1, NULL, 0),
(8, '100-112-02', 'Bank BRI PT 038501000889301', 'HARTA', NULL, 6, 5, 0, 1, 0, 1, NULL, 0),
(9, '100-112-91', 'Bank Danamon', 'HARTA', NULL, 6, 5, 0, 1, 0, 1, NULL, 0),
(10, '100-112-92', 'Ayat Silang', 'HARTA', NULL, 6, 5, 0, 1, 0, 1, NULL, 0),
(11, '100-12', 'DEPOSITO', 'HARTA', NULL, 2, 3, 1, 0, 0, 1, NULL, 0),
(12, '100-120-01', 'Deposito - on call', 'HARTA', 'Aktivitas Operasi', 11, 4, 0, 0, 0, 1, NULL, 0),
(13, '100-120-02', 'Deposito - Fixed Deposit', 'HARTA', 'Aktivitas Operasi', 11, 4, 0, 0, 0, 1, NULL, 0),
(14, '100-13', 'PIUTANG', 'HARTA', 'Aktivitas Operasi', 2, 3, 1, 0, 0, 1, NULL, 0),
(15, '100-131', 'PIUTANG USAHA', 'HARTA', 'Aktivitas Operasi', 14, 4, 1, 0, 0, 1, NULL, 0),
(16, '100-131-1', 'PIUTANG  SEWA', 'HARTA', 'Aktivitas Operasi', 15, 5, 1, 0, 0, 1, NULL, 0),
(17, '100-131-11', 'Piutang Sewa Pasar', 'HARTA', 'Aktivitas Operasi', 16, 6, 0, 0, 0, 1, NULL, 0),
(18, '100-131-12', 'Piutang Sewa Bazar', 'HARTA', 'Aktivitas Operasi', 16, 6, 0, 0, 0, 1, NULL, 0),
(19, '100-131-13', 'Piutang Sewa Both', 'HARTA', 'Aktivitas Operasi', 16, 6, 0, 0, 0, 1, NULL, 0),
(20, '100-131-14', 'Piutang Sewa Food Court', 'HARTA', 'Aktivitas Operasi', 16, 6, 0, 0, 0, 1, NULL, 0),
(21, '100-131-15', 'Piutang Sewa Corporate', 'HARTA', 'Aktivitas Operasi', 16, 6, 0, 0, 0, 1, NULL, 0),
(22, '100-131-2', 'PIUTANG  IURAN', 'HARTA', 'Aktivitas Operasi', 15, 5, 1, 0, 0, 1, NULL, 0),
(23, '100-131-21', 'Piutang Iuran Pasar', 'HARTA', 'Aktivitas Operasi', 22, 6, 0, 0, 0, 1, NULL, 0),
(24, '100-131-22', 'Piutang Iuran Bazar', 'HARTA', 'Aktivitas Operasi', 22, 6, 0, 0, 0, 1, NULL, 0),
(25, '100-131-23', 'Piutang Iuran Both', 'HARTA', 'Aktivitas Operasi', 22, 6, 0, 0, 0, 1, NULL, 0),
(26, '100-131-24', 'Piutang Iuran Food Court', 'HARTA', 'Aktivitas Operasi', 22, 6, 0, 0, 0, 1, NULL, 0),
(27, '100-132', 'PIUTANG NON USAHA', 'HARTA', 'Aktivitas Operasi', 14, 4, 1, 0, 0, 1, NULL, 0),
(28, '100-132-1', 'CASH ADVANCE', 'HARTA', 'Aktivitas Operasi', 27, 5, 1, 0, 0, 1, NULL, 0),
(29, '100-132-11', 'Cash Advance Operasional', 'HARTA', 'Aktivitas Operasi', 28, 6, 0, 0, 0, 1, NULL, 0),
(30, '100-132-12', 'Cash Advance Marketing', 'HARTA', 'Aktivitas Operasi', 28, 6, 0, 0, 0, 1, NULL, 0),
(31, '100-132-13', 'Cash Advance Proyek', 'HARTA', 'Aktivitas Operasi', 28, 6, 0, 0, 0, 1, NULL, 0),
(32, '100-132-2', 'PIUTANG  ANTAR DEVISI', 'HARTA', 'Aktivitas Operasi', 27, 5, 1, 0, 0, 1, NULL, 0),
(33, '100-132-21', 'Piutang A', 'HARTA', 'Aktivitas Operasi', 32, 6, 0, 0, 0, 1, NULL, 0),
(34, '100-132-22', 'Piutang B', 'HARTA', 'Aktivitas Operasi', 32, 6, 0, 0, 0, 1, NULL, 0),
(35, '100-132-3', 'PIUTANG LAIN-LAIN', 'HARTA', 'Aktivitas Operasi', 27, 5, 1, 0, 0, 1, NULL, 0),
(36, '100-132-31', 'Piutang Karyawan', 'HARTA', 'Aktivitas Operasi', 35, 6, 0, 0, 0, 1, NULL, 0),
(37, '100-132-32', 'Piutang P3', 'HARTA', 'Aktivitas Operasi', 35, 6, 0, 0, 0, 1, NULL, 0),
(38, '100-132-33', 'Piutang Lain-lain', 'HARTA', 'Aktivitas Operasi', 35, 6, 0, 0, 0, 1, NULL, 0),
(39, '100-14', 'PERSEDIAAN', 'HARTA', 'Aktivitas Operasi', 2, 3, 1, 0, 0, 1, NULL, 0),
(40, '100-140', 'PERSEDIAAN BARANG PROYEK', 'HARTA', 'Aktivitas Operasi', 39, 4, 1, 0, 0, 1, NULL, 0),
(41, '100-140-01', 'Persediaan Barang Sipil', 'HARTA', 'Aktivitas Operasi', 40, 5, 0, 0, 0, 1, NULL, 0),
(42, '100-140-02', 'Persediaan Barang ELektrikal', 'HARTA', 'Aktivitas Operasi', 40, 5, 0, 0, 0, 1, NULL, 0),
(43, '100-140-03', 'Persediaan Barang Lain-lain', 'HARTA', 'Aktivitas Operasi', 40, 5, 0, 0, 0, 1, NULL, 0),
(44, '100-150', 'PEMBANGUNAN DALAM PROSES', 'HARTA', 'Aktivitas Operasi', 2, 3, 1, 0, 0, 1, NULL, 0),
(45, '100-150-01', 'PDP Tanah', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(46, '100-150-02', 'PDP Persiapan Lahan', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(47, '100-150-03', 'PDP Perijinan, IMB, PBB', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(48, '100-150-04', 'PDP Design Arsitektur', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(49, '100-150-05', 'PDP Konsultan Amdalalin', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(50, '100-150-06', 'PDP Pembangunan', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(51, '100-150-07', 'PDP Pemb Jalan, Taman, Selokan', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(52, '100-150-08', 'PDP Instalasi Listrik, Pdam, Telpone', 'HARTA', 'Aktivitas Operasi', 44, 4, 0, 0, 0, 1, NULL, 0),
(53, '100-160', 'BIAYA DIBAYAR DIMUKA', 'HARTA', 'Aktivitas Operasi', 2, 3, 1, 0, 0, 1, NULL, 0),
(54, '100-160-01', 'Biaya Asuransi', 'HARTA', 'Aktivitas Operasi', 53, 4, 0, 0, 0, 1, NULL, 0),
(55, '100-160-02', 'Biaya Sewa', 'HARTA', 'Aktivitas Operasi', 53, 4, 0, 0, 0, 1, NULL, 0),
(56, '100-170', 'PAJAK DIBAYAR DIMUKA', 'HARTA', 'Aktivitas Operasi', 2, 3, 1, 0, 0, 1, NULL, 0),
(57, '100-170-01', 'PPn Masukkan', 'HARTA', 'Aktivitas Operasi', 56, 4, 0, 0, 0, 1, NULL, 0),
(58, '100-170-02', 'PPH 21 Dimuka', 'HARTA', 'Aktivitas Operasi', 56, 4, 0, 0, 0, 1, NULL, 0),
(59, '100-170-03', 'PPH 22 Dimuka', 'HARTA', 'Aktivitas Operasi', 56, 4, 0, 0, 0, 1, NULL, 0),
(60, '100-170-04', 'PPH 23 Dimuka', 'HARTA', 'Aktivitas Operasi', 56, 4, 0, 0, 0, 1, NULL, 0),
(61, '100-170-05', 'PPH Final 4 (2)', 'HARTA', 'Aktivitas Operasi', 56, 4, 0, 0, 0, 1, NULL, 0),
(62, '100-170-06', 'PPN Bayar', 'HARTA', 'Aktivitas Operasi', 56, 4, 0, 0, 0, 1, NULL, 0),
(63, '100-2', 'AKTIVA TETAP', 'HARTA', 'Aktivitas Operasi', 1, 2, 1, 0, 0, 1, NULL, 0),
(64, '100-210', 'NILAI BUKU', 'HARTA', 'Aktivitas Operasi', 63, 3, 1, 0, 0, 1, NULL, 0),
(65, '100-210-01', 'Investment', 'HARTA', 'Investasi', 64, 4, 0, 0, 0, 1, NULL, 0),
(66, '100-210-02', 'Tanah & Bangunan', 'HARTA', 'Investasi', 64, 4, 0, 0, 0, 1, NULL, 0),
(67, '100-210-03', 'Peralatan & Mesin Kantor', 'HARTA', 'Investasi', 64, 4, 0, 0, 0, 1, NULL, 0),
(68, '100-210-04', 'Perlengkapan & Furniture Kantor', 'HARTA', 'Investasi', 64, 4, 0, 0, 0, 1, NULL, 0),
(69, '100-210-05', 'Kendaraan', 'HARTA', 'Investasi', 64, 4, 0, 0, 0, 1, NULL, 0),
(70, '100-220', 'AKUMULASI PENYUSUTAN', 'HARTA', 'Aktivitas Operasi', 63, 3, 1, 0, 0, 1, NULL, 0),
(71, '100-220-01', 'Amortisasi Investment', 'HARTA', 'Aktivitas Operasi', 70, 4, 0, 0, 0, 1, NULL, 0),
(72, '100-220-02', 'Amortisasi Tanah & Bangunan', 'HARTA', 'Aktivitas Operasi', 70, 4, 0, 0, 0, 1, NULL, 0),
(73, '100-220-03', 'Ak Penystan Peralatan & Mesin Kantor', 'HARTA', 'Aktivitas Operasi', 70, 4, 0, 0, 0, 1, NULL, 0),
(74, '100-220-04', 'Ak Penystan Perlengkapan & Furniture Kantor', 'HARTA', 'Aktivitas Operasi', 70, 4, 0, 0, 0, 1, NULL, 0),
(76, '100-230', 'PRE-OPERATING', 'HARTA', 'Investasi', 63, 3, 0, 0, 0, 1, NULL, 0),
(77, '100-230-01', 'Ak Penystan Kendaraan', 'HARTA', 'Aktivitas Operasi', 70, 4, 0, 0, 0, 1, NULL, 0),
(78, '100-230-02', 'Amortization of - Pre Operating', 'HARTA', 'Investasi', 70, 4, 0, 0, 0, 1, '2019-12-04', 1),
(79, '200', 'HUTANG', 'KEWAJIBAN', 'Aktivitas Operasi', 0, 1, 1, 0, 1, -1, NULL, 0),
(80, '200-1', 'HUTANG LANCAR', 'KEWAJIBAN', 'Aktivitas Operasi', 79, 2, 1, 0, 0, -1, NULL, 0),
(81, '200-10', 'HUTANG USAHA', 'KEWAJIBAN', 'Aktivitas Operasi', 80, 3, 1, 0, 0, -1, NULL, 0),
(82, '200-110', 'HUTANG VENDOR', 'KEWAJIBAN', 'Aktivitas Operasi', 81, 4, 1, 0, 0, -1, NULL, 0),
(83, '200-110-01', 'Hutang Subkon Proyek', 'KEWAJIBAN', 'Aktivitas Operasi', 82, 5, 0, 0, 0, -1, NULL, 0),
(84, '200-110-02', 'Hutang Supplier Marketing', 'KEWAJIBAN', 'Aktivitas Operasi', 82, 5, 0, 0, 0, -1, NULL, 0),
(85, '200-110-03', 'Hutang Supplier Operasional', 'KEWAJIBAN', 'Aktivitas Operasi', 82, 5, 0, 0, 0, -1, NULL, 0),
(86, '200-110-04', 'Hutang Mandor', 'KEWAJIBAN', 'Aktivitas Operasi', 82, 5, 0, 0, 0, -1, NULL, 0),
(87, '200-120', 'HUTANG Non Vendor', 'KEWAJIBAN', 'Aktivitas Operasi', 81, 4, 1, 0, 0, -1, NULL, 0),
(88, '200-120-01', 'Hutang Komisi', 'KEWAJIBAN', 'Aktivitas Operasi', 87, 5, 0, 0, 0, -1, NULL, 0),
(89, '200-120-02', 'Hutang Leasing', 'KEWAJIBAN', 'Aktivitas Operasi', 87, 5, 0, 0, 0, -1, NULL, 0),
(90, '200-130-00', 'HUTANG BANK', 'KEWAJIBAN', 'Aktivitas Operasi', 81, 4, 0, 0, 0, -1, '2019-12-04', 1),
(91, '200-130-01', 'Hutang Bank A', 'KEWAJIBAN', 'Aktivitas Operasi', 81, 1, 0, 0, 1, -1, '2019-12-04', 1),
(92, '200-20', 'HUTANG DILUAR USAHA', 'KEWAJIBAN', 'Aktivitas Operasi', 80, 3, 1, 0, 0, -1, NULL, 0),
(93, '200-210', 'HUTANG BIAYA', 'KEWAJIBAN', 'Aktivitas Operasi', 92, 4, 1, 0, 0, -1, NULL, 0),
(94, '200-210-01', 'Hutang Biaya Gaji', 'KEWAJIBAN', 'Aktivitas Operasi', 93, 5, 0, 0, 0, -1, NULL, 0),
(95, '200-210-02', 'Hutang Biaya Listrik, Air, telpone', 'KEWAJIBAN', 'Aktivitas Operasi', 93, 5, 0, 0, 0, -1, NULL, 0),
(96, '200-220', 'HUTANG ASURANSI,JAMINAN', 'KEWAJIBAN', 'Aktivitas Operasi', 92, 4, 1, 0, 0, -1, NULL, 0),
(97, '200-220-01', 'Hutang Asuransi', 'KEWAJIBAN', 'Aktivitas Operasi', 96, 5, 0, 0, 0, -1, NULL, 0),
(98, '200-220-02', 'Hutang Jaminan', 'KEWAJIBAN', 'Aktivitas Operasi', 96, 5, 0, 0, 0, -1, NULL, 0),
(99, '200-230', 'HUTANG LAIN-LAIN', 'KEWAJIBAN', 'Aktivitas Operasi', 92, 4, 1, 0, 0, -1, NULL, 0),
(100, '200-230-01', 'Hutang Direksi / P3', 'KEWAJIBAN', 'Aktivitas Operasi', 99, 5, 0, 0, 0, -1, NULL, 0),
(101, '200-230-02', 'Hutang Devisi Lain', 'KEWAJIBAN', 'Aktivitas Operasi', 99, 5, 0, 0, 0, -1, NULL, 0),
(102, '200-230-03', 'Hutang Bunga Pinjaman', 'KEWAJIBAN', 'Aktivitas Operasi', 99, 5, 0, 0, 0, -1, NULL, 0),
(103, '200-240', 'HUTANG PAJAK', 'KEWAJIBAN', 'Aktivitas Operasi', 92, 4, 1, 0, 0, -1, NULL, 0),
(104, '200-240-01', 'PPN Keluaran Rp', 'KEWAJIBAN', 'Aktivitas Operasi', 103, 5, 0, 0, 0, -1, NULL, 0),
(105, '200-240-02', 'PPH 21 Karyawan', 'KEWAJIBAN', 'Aktivitas Operasi', 103, 5, 0, 0, 0, -1, NULL, 0),
(106, '200-240-03', 'PPH 23, 4 (2) Marketing', 'KEWAJIBAN', 'Aktivitas Operasi', 103, 5, 0, 0, 0, -1, NULL, 0),
(107, '200-240-04', 'PPH 23, 4 (2) Suplier/Kontraktor ', 'KEWAJIBAN', 'Aktivitas Operasi', 103, 5, 0, 0, 0, -1, NULL, 0),
(108, '200-240-05', 'PPH 4 (2) Rumah/Gud/Aprt', 'KEWAJIBAN', 'Aktivitas Operasi', 103, 5, 0, 0, 0, -1, NULL, 0),
(109, '200-240-06', 'PPH 25 Badan', 'KEWAJIBAN', 'Aktivitas Operasi', 103, 5, 0, 0, 0, -1, NULL, 0),
(110, '200-30', 'PENDAPATAN DITANGGUHKAN', 'KEWAJIBAN', 'Aktivitas Operasi', 80, 3, 1, 0, 0, -1, NULL, 0),
(111, '200-310', 'TITIPAN SEWA', 'KEWAJIBAN', 'Aktivitas Operasi', 110, 4, 1, 0, 0, -1, NULL, 0),
(112, '200-310-01', 'Titipan Dana Simpanan', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(113, '200-310-02', 'Titipan Deposito', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(114, '200-310-03', 'Titipan Sewa Pasar', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(115, '200-310-04', 'Titipan Sewa Bazar', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(116, '200-310-05', 'Titipan Sewa Food Court', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(117, '200-310-06', 'Titipan Sewa Corporate', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(118, '200-320', 'TITIPAN IURAN', 'KEWAJIBAN', 'Aktivitas Operasi', 110, 4, 1, 0, 0, -1, NULL, 0),
(119, '200-320-01', 'Titipan Iuran Service Charge', 'KEWAJIBAN', 'Aktivitas Operasi', 118, 5, 0, 0, 0, -1, NULL, 0),
(120, '300', 'MODAL', 'MODAL', 'Investasi', 0, 1, 1, 0, 1, -1, NULL, 0),
(121, '300-110', 'MODAL', 'MODAL', 'Investasi', 120, 2, 1, 0, 0, -1, NULL, 0),
(122, '300-110-01', 'Setoran Modal', 'MODAL', 'Investasi', 121, 3, 0, 0, 0, -1, NULL, 0),
(123, '300-110-02', 'Laba Ditahan', 'MODAL', 'Investasi', 121, 3, 0, 0, 0, -1, NULL, 0),
(124, '300-110-03', 'Laba ( Rugi ) Tahun Lalu', 'MODAL', 'Investasi', 121, 3, 0, 0, 0, -1, NULL, 0),
(125, '300-110-04', 'Laba ( Rugi ) Tahun Berjalan', 'MODAL', 'Investasi', 121, 3, 0, 0, 0, -1, NULL, 0),
(126, '300-110-05', 'Dividen', 'MODAL', 'Investasi', 121, 3, 0, 0, 0, -1, NULL, 0),
(127, '400', 'PENDAPATAN USAHA', 'PENDAPATAN', 'Aktivitas Operasi', 0, 1, 1, 0, 1, -1, NULL, 0),
(128, '400-110', 'PENDAPATAN SEWA BANGUNAN', 'PENDAPATAN', 'Aktivitas Operasi', 127, 2, 1, 0, 0, -1, NULL, 0),
(129, '400-110-01', 'Pendapatan Sewa Pasar', 'PENDAPATAN', 'Aktivitas Operasi', 128, 3, 0, 0, 0, -1, NULL, 0),
(130, '400-110-02', 'Pendapatan Sewa Bazar', 'PENDAPATAN', 'Aktivitas Operasi', 128, 3, 0, 0, 0, -1, NULL, 0),
(131, '400-110-03', 'Pendapatan Sewa Both', 'PENDAPATAN', 'Aktivitas Operasi', 128, 3, 0, 0, 0, -1, NULL, 0),
(132, '400-110-04', 'Pendapatan Sewa Food Court', 'PENDAPATAN', 'Aktivitas Operasi', 128, 3, 0, 0, 0, -1, NULL, 0),
(133, '400-110-05', 'Pendapatan Sewa Corporate', 'PENDAPATAN', 'Aktivitas Operasi', 128, 3, 0, 0, 0, -1, NULL, 0),
(134, '400-120', 'PENDAPATAN IURAN', 'PENDAPATAN', 'Aktivitas Operasi', 127, 2, 1, 0, 0, -1, NULL, 0),
(135, '400-120-01', 'Pendapatan Iuran Pasar', 'PENDAPATAN', 'Aktivitas Operasi', 134, 3, 0, 0, 0, -1, NULL, 0),
(136, '400-120-02', 'Pendapatan Iuran Bazar', 'PENDAPATAN', 'Aktivitas Operasi', 134, 3, 0, 0, 0, -1, NULL, 0),
(137, '400-120-03', 'Pendapatan Iuran Both', 'PENDAPATAN', 'Aktivitas Operasi', 134, 3, 0, 0, 0, -1, NULL, 0),
(138, '400-120-04', 'Pendapatan Iuran Food Court', 'PENDAPATAN', 'Aktivitas Operasi', 134, 3, 0, 0, 0, -1, NULL, 0),
(139, '500', 'HARGA POKOK PENJUALAN', 'BEBAN', 'Aktivitas Operasi', 0, 1, 1, 0, 1, 1, NULL, 0),
(140, '500-110', 'HPP SEWA', 'BEBAN', 'Aktivitas Operasi', 139, 2, 1, 0, 0, 1, NULL, 0),
(141, '500-110-01', 'HPP Bangunan', 'BEBAN', 'Aktivitas Operasi', 140, 3, 0, 0, 0, 1, NULL, 0),
(142, '500-110-02', 'HPP Prasarana', 'BEBAN', 'Aktivitas Operasi', 140, 3, 0, 0, 0, 1, NULL, 0),
(143, '500-120', 'HPP IURAN', 'BEBAN', 'Aktivitas Operasi', 139, 2, 1, 0, 0, 1, NULL, 0),
(144, '500-120-01', 'HPP Pengelolah', 'BEBAN', 'Aktivitas Operasi', 143, 3, 0, 0, 0, 1, NULL, 0),
(145, '500-120-02', 'HPP Kebersihan', 'BEBAN', 'Aktivitas Operasi', 143, 3, 0, 0, 0, 1, NULL, 0),
(146, '500-120-03', 'HPP Keamanan', 'BEBAN', 'Aktivitas Operasi', 143, 3, 0, 0, 0, 1, NULL, 0),
(147, '500-120-04', 'HPP LISTRIK, Air', 'BEBAN', 'Aktivitas Operasi', 143, 3, 0, 0, 0, 1, NULL, 0),
(148, '500-120-05', 'HPP Pemeliharaan', 'BEBAN', 'Aktivitas Operasi', 143, 3, 0, 0, 0, 1, NULL, 0),
(149, '600', 'BIAYA ADMINISTRASI & UMUM', 'BEBAN', 'Aktivitas Operasi', 0, 1, 1, 0, 1, 1, NULL, 0),
(150, '600-110', 'BIAYA MARKETING', 'BEBAN', 'Aktivitas Operasi', 149, 2, 1, 0, 0, 1, NULL, 0),
(151, '600-110-01', 'Promosi Via Banner,Brosur, Billboard', 'BEBAN', 'Aktivitas Operasi', 150, 3, 0, 0, 0, 1, NULL, 0),
(152, '600-110-02', 'Promosi Via Even,Pameran dll', 'BEBAN', 'Aktivitas Operasi', 150, 3, 0, 0, 0, 1, NULL, 0),
(153, '600-110-03', 'Promosi Via Koran, Radio, Sosmed', 'BEBAN', 'Aktivitas Operasi', 150, 3, 0, 0, 0, 1, NULL, 0),
(154, '600-110-04', 'Komisi Marketing', 'BEBAN', 'Aktivitas Operasi', 150, 3, 0, 0, 0, 1, NULL, 0),
(155, '600-110-05', 'Oprs, Admin Marketing', 'BEBAN', 'Aktivitas Operasi', 150, 3, 0, 0, 0, 1, NULL, 0),
(156, '600-120', 'BIAYA ADMINISTRASI & UMUM', 'BEBAN', 'Aktivitas Operasi', 149, 2, 1, 0, 0, 1, NULL, 0),
(157, '600-120-01', 'Biaya Gaji', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(158, '600-120-02', 'Tunjangan PPH 21', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(159, '600-120-03', 'Tunjangan BPJS', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(160, '600-120-04', 'SDM ( Mess, Transport, Plthan )', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(161, '600-120-05', 'Alat Tulis, F copy, Perlengkapan Kantor ', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(162, '600-120-06', 'Konsumsi, Keperluan RT Kantor', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(163, '600-120-07', 'Postage, Mail & Courier', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(164, '600-120-08', 'Handphone, Telp, Internet', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(165, '600-120-09', 'Listrik Kantor', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(166, '600-120-10', 'Fuel,Parking &Toll', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(167, '600-120-11', 'Pemeliharaan Kantor, Inventaris', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(168, '600-120-12', 'DLK ( Tiket, Hotel, Akmodosi )', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(169, '600-120-13', 'Legalitas, Konsultan', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(170, '600-120-14', 'Operasional Kantor', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(171, '600-120-15', 'Entertaiment', 'BEBAN', 'Aktivitas Operasi', 156, 3, 0, 0, 0, 1, NULL, 0),
(172, '700', 'PENDAPATAN / PENGELUARAN DILUAR USAHA', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 0, 1, 1, 0, 1, -1, NULL, 0),
(173, '700-110', 'PENDAPATAN LAIN-LAIN', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 172, 2, 1, 0, 0, -1, NULL, 0),
(174, '700-110-01', 'Pendapatan Bunga , Denda', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 173, 3, 0, 0, 0, -1, NULL, 0),
(175, '700-110-02', 'Pendapatan Jasa Giro', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 173, 3, 0, 0, 0, -1, NULL, 0),
(176, '700-110-03', 'Pendapatan Bunga Deposito', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 173, 3, 0, 0, 0, -1, NULL, 0),
(177, '700-110-04', 'Pendapatan Non Usaha', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 173, 3, 0, 0, 0, -1, NULL, 0),
(178, '700-110-05', 'Pendapatan Others', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 173, 3, 0, 0, 0, -1, NULL, 0),
(179, '700-120', 'PENGELUARAN LAIN-LAIN', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 172, 2, 1, 0, 0, -1, NULL, 0),
(180, '700-120-01', 'Biaya Lost F/A', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 179, 3, 0, 0, 0, -1, NULL, 0),
(181, '700-120-02', 'Biaya Admin Bank', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 179, 3, 0, 0, 0, -1, NULL, 0),
(182, '700-120-03', 'Biaya Bunga Pinjaman Bank', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 179, 3, 0, 0, 0, -1, NULL, 0),
(183, '700-120-04', 'Biaya Bunga Pinjaman P3', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 179, 3, 0, 0, 0, -1, NULL, 0),
(184, '700-130', 'BIAYA PENYUSUTAN', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 172, 2, 1, 0, 0, -1, NULL, 0),
(185, '700-130-01', 'BIAYA PENYUSUTAN', 'PENDAPATAN DILUAR USAHA', 'Aktivitas Operasi', 184, 3, 0, 0, 0, -1, NULL, 0),
(186, '800', 'TAX EXPENSES', 'BEBAN DILUAR USAHA', 'Aktivitas Operasi', 0, 1, 1, 0, 1, 1, NULL, 0),
(187, '800-110', 'BIAYA PAJAK', 'BEBAN DILUAR USAHA', 'Aktivitas Operasi', 186, 2, 1, 0, 0, 1, NULL, 0),
(188, '800-110-01', 'PPH Final 4 (2)', 'BEBAN DILUAR USAHA', 'Aktivitas Operasi', 187, 3, 0, 0, 0, 1, NULL, 0),
(189, '100-112-03', 'Bank BRI CV 038501000888305', 'HARTA', NULL, 6, 5, 0, 1, 0, 1, NULL, 0),
(190, '100-131-25', 'Piutang Iuran Corporate', 'HARTA', NULL, 22, 6, 0, 0, 0, 1, NULL, 0),
(191, '200-310-11', 'Titipan Sewa Parkir', 'KEWAJIBAN', 'Aktivitas Operasi', 111, 5, 0, 0, 0, -1, NULL, 0),
(192, '400110', 'PENDAPATAN USAHA', 'MODAL', NULL, 127, 2, 1, 0, NULL, -1, '2020-01-14', 1),
(193, '400-130', 'PENDAPATAN SEWA LAHAN', 'PENDAPATAN', NULL, 127, 2, 1, 0, NULL, -1, NULL, 0),
(194, '400-150', 'PENDAPATAN EVEN', 'PENDAPATAN', NULL, 127, 2, 1, 0, NULL, -1, NULL, 0),
(195, '400-130-01', 'Pendapatan Sewa Lahan Parkir', 'PENDAPATAN', NULL, 193, 3, 0, 0, 0, -1, NULL, 0),
(196, '400-130-02', 'Pendapatan Sewa Lahan Wahana', 'PENDAPATAN', NULL, 193, 3, 0, 0, 0, -1, NULL, 0),
(197, '400-150-01', 'Pendapatan Even Bazar', 'PENDAPATAN', NULL, 194, 3, 0, 0, 0, -1, NULL, 0),
(198, '400-150-02', 'Pendapatan Even Wahana', 'PENDAPATAN', NULL, 194, 3, 0, 0, 0, -1, NULL, 0),
(199, '100-210-06', 'Perlengkapan Bazar dan Food Court', 'HARTA', NULL, 64, 4, 0, 1, 0, 1, NULL, 0),
(200, '200-210-03', 'Hutang Biaya Operasional', 'KEWAJIBAN', NULL, 93, 5, 0, 0, 0, -1, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_m_akun`
--
ALTER TABLE `acc_m_akun`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_m_akun`
--
ALTER TABLE `acc_m_akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
