CREATE TABLE `t_surat` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `no_surat` varchar(100) NULL,
  `asal_surat` varchar(255) NULL,
  `isi` text NULL,
  `tgl_surat` date NULL,
  `tgl_diterima` date NULL,
  `file` text NULL,
  `keterangan` text NULL,
  `jenis` varchar(45) NULL,
  `created_at` int NULL,
  `created_by` int NULL,
  `modified_at` int NULL,
  `modified_by` int NULL
);
CREATE TABLE `t_surat_keterangan` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `no_surat` varchar(255) NULL,
  `penduduk_id` int NULL,
  `surat_id` int NULL,
  `keterangan` text NULL,
  `created_at` int NULL,
  `created_by` int NULL,
  `modified_at` int NULL,
  `modified_by` int NULL
);