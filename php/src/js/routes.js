angular.module("app").config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider", "$breadcrumbProvider",
    function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
        $urlRouterProvider.otherwise("/artikel");
        var cache = false;
        if (cache == true) {
            var timecache = null;
        } else {
            var timecache = new Date().getTime();
        }
        $ocLazyLoadProvider.config({
            debug: false
        });
        $breadcrumbProvider.setOptions({
            // prefixStateName: "app.main",
            includeAbstract: true,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
        });
        $stateProvider.state("app", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Root",
                skip: true
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
            }
        }).state("app.main", {
            url: "/dashboard",
            templateUrl: "tpl/dashboard/dashboard.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Dashboard"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js","zingchart-angularjs"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/dashboard/dashboard.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.artikel", {
            url: "/artikel",
            templateUrl: "./tpl/m_artikel/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["./tpl/m_artikel/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.kategori_artikel", {
            url: "/kategori-artikel",
            templateUrl: "./tpl/m_kategori_artikel/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kategori Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["./tpl/m_kategori_artikel/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.gallery", {
            url: "/gallery",
            templateUrl: "./tpl/m_gallery/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Gallery"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64","angularFileUpload", "daterangepicker"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["./tpl/m_gallery/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("app.kategori_galeri", {
            url: "/kategori_galeri",
            templateUrl: "./tpl/m_kategori_galeri/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Kategori Gallery"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["naif.base64", "daterangepicker", "angularFileUpload"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["./tpl/m_kategori_galeri/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("pengguna", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "User Login"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        // return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("pengguna.profil", {
            url: "/profil",
            templateUrl: "tpl/m_user/profile.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Profil Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/profile.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("setting", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Setting"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("setting.setting", {
            url: "/setting-app",
            templateUrl: "tpl/m_setting/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Setting App"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_setting/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("master", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Data Master"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function ($ocLazyLoad) {
                }],
            }
        }).state("master.akses", {
            url: "/hak-akses",
            templateUrl: "tpl/m_akses/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Hak Akses"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_akses/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("master.user", {
            url: "/user",
            templateUrl: "tpl/m_user/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/index.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("master.setting_aplikasi", {
            url: "/setting-aplikasi",
            templateUrl: "tpl/m_setting/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Setting Aplikasi"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_setting/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("master.surat", {
            url: "/surat",
            templateUrl: "tpl/m_surat/index.html?time=" + timecache,
            ncyBreadcrumb: {
                label: "Surat"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/m_surat/index.js?time=" + timecache]
                            });
                        });
                    }
                ]
            }
        }).state("page", {
            abstract: true,
            templateUrl: "tpl/common/layouts/blank.html?time=" + timecache,
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ]
            }
        }).state("page.login", {
            url: "/login",
            templateUrl: "tpl/common/pages/login.html?time=" + timecache,
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/login.js?time=" + timecache]
                        });
                    }
                ]
            }
        }).state("page.404", {
            url: "/404",
            templateUrl: "tpl/common/pages/404.html"
        }).state("page.500", {
            url: "/500",
            templateUrl: "tpl/common/pages/500.html"
        });
        // function authenticate($q, UserService, $state, $transitions, $location, $rootScope) {
        //     var deferred = $q.defer();
        //     if (UserService.isAuth()) {
        //         deferred.resolve();
        //         var fromState = $state;
        //         var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator"];
        //         $transitions.onStart({}, function ($transition$) {
        //             var toState = $transition$.$to();
        //             if ($rootScope.user.akses[toState.name.replace(".", "_")] || globalmenu.indexOf(toState.name)) {
        //                 // if (UserService.getPerusahaan() == null || UserService.getPerusahaan() == undefined) {
        //                 //     $location.path("/s-perusahaan");
        //                 // }
        //                 // if (UserService.getProyek() == null || UserService.getProyek() == undefined) {
        //                 //     $location.path("/s-proyek");
        //                 // }
        //             } else {
        //                 $state.target("page.500")
        //             }
        //         });
        //     } else {
        //         $location.path("/login");
        //     }
        //     return deferred.promise;
        // }
    }
]).run(["$rootScope", "$state", "$stateParams", "$transitions", "UserService", "$location", "$q", "Data", "toaster",
    function ($rootScope, $state, $stateParams, $transitions, UserService, $location, $q, Data, toaster) {
        // $transitions.onSuccess({}, function () {
        //     document.body.scrollTop = document.documentElement.scrollTop = 0;
        // });
        $rootScope.user = UserService.getUser();
        $rootScope.$state = $state;
        /** Datepicker Options */
        $rootScope.opened = {};
        $rootScope.toggle = function ($event, elemId) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.opened[elemId] = !$rootScope.opened[elemId];
        };
        /** Daterange Options */
        // $rootScope.dateRangeOptions = {
        //     locale: {
        //         format: "DD-MM-YYYY",
        //         separator: " s/d "
        //     }
        // };

        $rootScope.dateRangeOptions = {
            // locale: {
            //     format: "DD-MM-YYYY",
            //     separator: " s/d "
            // }

            linkedCalendars: false,
            ranges: {
                'Hari Ini': [moment(), moment()],
                'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 Hari Lalu': [moment().subtract(6, 'days'), moment()],
                '30 Hari Lalu': [moment().subtract(29, 'days'), moment()],
                'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            alwaysShowCalendars: true,
            autoApply: true,
            locale: {
                format: "DD-MM-YYYY",
                separator: " s/d "
            }
        };
        /** Sweet Alert */
        $rootScope.alert = function (judul, text, tipe = "warning", target = "app.sPerusahaan, laporan.sewa.status_unit, laporan.sewa.jadwal_tagihan, laporan.kepemilikan.biaya, laporan.kepemilikan.serah_terima, laporan.overtime.overtime ") {
            if (tipe == "success") {
                Swal.fire({
                    title: '<strong>' + judul + '</strong>',
                    html: text,
                    type: tipe,
                    onOpen: function () {
                        Swal.showLoading()
                        // AJAX request simulated with setTimeout
                        setTimeout(function () {
                            Swal.close()
                        }, 1500)
                    }
                }).then((close) => {
                    if (close) {
                    } else {
                    }
                });
            } else if (tipe == "setting") {
                Swal.fire({
                    title: '<strong>' + judul + '</strong>',
                    html: text,
                    type: "warning",
                }).then((close) => {
                    if (close) {
                        $state.go(target)
                    }
                });
            } else if (tipe == "data_kosong") {
                Swal.fire({
                    title: '<strong>Data Tidak Ditemukan</strong>',
                    html: 'Data <b>' + text + '</b> sesuai filter yang anda masukkan tidak ditemukan.',
                    type: "warning",
                }).then((close) => {
                    if (close) {
                    }
                });
            } else {
                Swal.fire({
                    title: '<strong>' + judul + '</strong>',
                    html: text,
                    type: tipe,
                }).then((close) => {
                    if (close) {
                    } else {
                    }
                });
            }
        };

        /**
         * GLOBAL GET DATA
         */
        $rootScope.getKabupaten = function (kabupaten, tipe) {
            if (tipe == "search") {
                if (kabupaten.toString().length > 2) {
                    Data.get("get_data2/kabupaten", {
                        'kabupaten': kabupaten
                    }).then(function (response) {
                        if (response.data.list.length > 0) {
                            $rootScope.listKabupaten = response.data.list;
                        } else {
                            toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kabupaten + "' Tidak Ditemukan");
                        }
                    });
                }
            }
        };

        $rootScope.getKecamatanWithKabupaten = function (kecamatan, kabupaten_id, kecamatan_id, tipe) {
            if (tipe == "search") {
                if (kecamatan.toString().length > 2) {
                    Data.get("get_data2/kecamatanWithKabupaten", {
                        'kecamatan': kecamatan
                    }).then(function (response) {
                        if (response.data.list.length > 0) {
                            $rootScope.listKecamatanWithKabupaten = response.data.list;
                        } else {
                            toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                        }
                    });
                }
            } else {
                Data.get("get_data2/kecamatanWithKabupaten", {
                    'kecamatan_id': kecamatan_id,
                    'kabupaten_id': kabupaten_id
                }).then(function (response) {
                    if (response.data.list.length > 0) {
                        $rootScope.listKecamatanWithKabupaten = response.data.list;
                    } else {
                        toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                    }
                });
            }
        }

        $rootScope.getDesa = function (desa, kecamatan_id, tipe, desa_id = null) {
            if (tipe == "search") {
                if (desa.toString().length > 2) {
                    Data.get("get_data2/desa", {
                        'desa': desa,
                        'kecamatan_id': kecamatan_id
                    }).then(function (response) {
                        if (response.data.list.length > 0) {
                            $rootScope.listDesa = response.data.list;
                        } else {
                            toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                        }
                    });
                }
            } else {
                Data.get("get_data2/desa", {
                    'kecamatan_id': kecamatan_id,
                    'desa_id': desa_id,
                }).then(function (response) {
                    if (response.data.list.length > 0) {
                        $rootScope.listDesa = response.data.list;
                    } else {
                        toaster.pop('warning', "Data Tidak Ditemukan", "Data dengan pencarian '" + kecamatan + "' Tidak Ditemukan");
                    }
                });
            }
        }

        $rootScope.getPerangkatDesa = function (desa_id) {
            Data.get('get_data2/perangkatDesa', {
                'desa_id': desa_id
            }).then(function (response) {
                $rootScope.listPerangkatDesa = response.data.list;
            })
        }

        $rootScope.getTandaTangan = function (desa_id) {
            Data.get('get_data2/tandaTangan', {}).then(function (response) {
                $rootScope.listTandaTangan = response.data.list;
            })
        }

        /**
         * PENGECEKAN AKSES DAN LOGIN
         */
        if (UserService.isAuth()) {
            var fromState = $state;
            var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator", "app.sPerusahaan", "app.sProyek"];
            $transitions.onStart({
                to: function (state) {
                    // PENGECEKAN SESSION
                    if (state.name != "page.login") {
                        Data.get("site/session").then(function (response) {
                            if (response.status_code == 422) {
                                $state.go("page.login")
                            } else {
                                UserService.setUser(result.data.user);
                                $rootScope.user = UserService.getUser();
                            }
                        });
                    }
                }
            }, function ($transition$) {
                var toState = $transition$.$to();
                if ($rootScope.user.akses[toState.name.replace(/\./g, "_")] == true || globalmenu.indexOf(toState.name) > -1) {

                } else {
                    $state.go("page.500")
                }
            });
        } else {
            $location.path("/login");
            // $state.go("page.login")
        };
        return ($rootScope.$stateParams = $stateParams);
    }
]);
