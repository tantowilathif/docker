app.controller("msettingaplikasiCtrl", function ($scope, Data, $rootScope, UserService) {
    /**
     * Inialisasi
     */
    //
    // $scope.getClient = UserService.getClient();
    // clientId = $rootScope.client.id;

    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */

    Data.get("m_setting/index").then(function (response) {
        $scope.form = response.data.list[0];
    });

    Data.get("get_data/kategori").then(function(response) {
        $scope.dataKategori = response.data;
    });


    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("m_setting/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $rootScope.user.setting_aplikasi = result.data;
                // window.location.reload();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };

});
