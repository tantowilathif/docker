app.controller("kategoriGaleriCtrl", function ($scope, Data, $rootScope, $uibModal) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    var control_link = "m_kategori_galeri";
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get(control_link + "/index", param).then(function (response) {
            $scope.displayed = response.data.list;

            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.password = "";
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.password = "";
    };
    $scope.save = function (form) {
        $scope.loading = false;
        Data.post(control_link + "/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            row.is_deleted = 1;
            Data.post(control_link + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANEN item ini ?")) {
            // console.log(row);return;

            Data.post(control_link + "/delete", row).then(function (result) {
                if (result.status_code == 200) {
                    alert("Berhasil dihapus");
                    $scope.callServer(tableStateRef);
                }
            });
        }
    };
    $scope.restore = function (row) {
        if (confirm("Apa anda yakin akan MERESTORE item ini ?")) {
            row.is_deleted = 0;
            Data.post(control_link + "/saveStatus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };


    $scope.modal = function (form) {
        var date = new Date();
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_kategori_galeri/modal.html?" + date,
            controller: "modalCtrl",
            size: "xl",
//            backdrop: "static",
            keyboard: false,
            scope: $scope,
            resolve: {
                form: form,
            }
        });
        modalInstance.result.then(function (response) {

        });
    }
});


app.controller("modalCtrl", function ($state, $scope, Data, $uibModalInstance, FileUploader, $uibModal, $rootScope, form, $sce) {
    $scope.form = form;
    $scope.activeForm = 0;
    $scope.is_create = false;


    /**
     * media gambar
     */
    console.log(form);

    $scope.gambar = [];

    var nmcontroller = "m_gallery";

    var uploaderGallery = $scope.uploaderGallery = new FileUploader({
        url: Data.base + nmcontroller + '/uploadGallery',
        formData: [],
        removeAfterUpload: true,
    });

    $scope.uploadGambar = function (form) {
        $scope.uploaderGallery.uploadAll();
    };

    uploaderGallery.filters.push({
        name: 'imageFilter',
        fn: function (item) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            var x = '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            if (!x) {
                $rootScope.alert("Terjadi Kesalahan", "Jenis gambar tidak sesuai", "error");
            }
            return x;
        }
    });

    uploaderGallery.filters.push({
        name: 'sizeFilter',
        fn: function (item) {
            var xz = item.size < 3097152;
            if (!xz) {
//                toaster.pop('error', "Ukuran gambar tidak boleh lebih dari 3 MB");
                $rootScope.alert("Terjadi Kesalahan", "Ukuran gambar tidak boleh lebih dari 3 MB", "error");
            }
            return xz;
        }
    });

    var form = $scope.form;

    $scope.gambarBro = function (val) {
        Data.get("m_gallery/dokumentasiGallery", val).then(function (response) {
            if (response.status_code == 422) {
                $scope.listGambar = [];
            } else {
                $scope.listGambar = response.data.list;
            }
        });
    }
    $scope.gambarBro(form);

    uploaderGallery.onSuccessItem = function (fileItem, response) {
        if (response.answer == 'File transfer completed') {
            $scope.listGambar.unshift({file: response.img, id: response.id});
            $scope.gambarBro(form);
        }
    };

    uploaderGallery.onBeforeUploadItem = function (item) {
//        console.log(item);
        item.formData.push({
            id: 1,
            kategori_id: form.id
        });
    };

    $scope.removeFoto = function (paramindex, namaFoto, pid, m_project_id) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            Data.post(nmcontroller + '/removegambar', {
                id: pid,
                file: namaFoto,
                m_project_id: m_project_id
            }).then(function (data) {
                $scope.listGambar.splice(paramindex, 1);
            });
        }

    };

    $scope.gambarzoom = function (id, img) {
        var modalInstance = $uibModal.open({
            template: '<img src="img/gallery/' + img + '" class="img-full" >',
            size: 'md',
        });
    };

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };

    $scope.primaryFoto = function (kategori_id, id, v) {
        console.log(kategori_id);
        if (confirm("Apa anda yakin akan Menjadikan Primary item ini ?")) {
            Data.post(nmcontroller + '/primarygambar', {
                kategori_id: kategori_id,
                id: id,
                is_primary: v
            }).then(function (data) {
//                $scope.gambarMedia(data);
                $scope.gambarBro(form);
            });
        }

    };


    /**
     * media video
     */


    $scope.cekLink = function (v, index) {
        console.log('aaaa');
        var cek = $scope.listDetailVideo[index].youtube_id;
        if (cek.length == 11) {
            var set = cek;
        } else {
            var set = cek.substring(cek.length - 11, cek.length);
        }
        $scope.listDetailVideo[index].thumbnail = 'http://img.youtube.com/vi/' + set + '/mqdefault.jpg';

    }


    $scope.listDetailVideo = [];
    $scope.getVideo = function (val) {
        Data.get("m_gallery/dokumentasiVideo", {id: val}).then(function (response) {
            if (response.status_code == 422) {
                $scope.listDetailVideo = [];
            } else {
                $scope.listDetailVideo = response.data.list;
                angular.forEach($scope.listDetailVideo, function (val, key) {
                    val.youtube = val.youtube_id;
                    val.thumbnail = 'http://img.youtube.com/vi/' + val.youtube_id + '/maxresdefault.jpg';
                })
            }
        });
    }

    $scope.getVideo(form.id);

    $scope.addDetail = function (val) {
        var newDet = {
        };
        val.push(newDet);
    };
    $scope.removeDetail = function (val, paramindex) {
        var comArr = eval(val);
        if (comArr.length > 1) {
            val.splice(paramindex, 1);
        } else {
            val.splice(paramindex, 1);
        }
    };

    $scope.convert = function (v,index) {
        if (v.length == 11) {
            $scope.listDetailVideo[index].youtube_id = v;
        } else {
            $scope.listDetailVideo[index].youtube_id = v.substring(v.length - 11, v.length);
        }
    }

        $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.save = function (form) {
//        $scope.loading = true;
        var params = {
            data: $scope.form,
            video: $scope.listDetailVideo
        }
//        console.log(form);
        Data.post("m_kategori_galeri/saveVideo", params).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil tersimpan", "success");
                $scope.cancel();
                $scope.is_create = false;
                $scope.activeForm = 1;
                $scope.getVideo(form.id);
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };

    $scope.tambah = function (form) {
        $scope.is_create = true;
    }

    $scope.cancel = function (form) {
        $scope.is_create = false;
        $scope.activeForm = 1;
    }

    $scope.modalYoutube = function (form, $index) {
        // console.log(form);return;
        var modalInstance = $uibModal.open({
            templateUrl: "tpl/m_kategori_galeri/youtube.html",
            controller: "modalYoutube",
            size: "lg",
//            backdrop: "static",
            keyboard: false,
            resolve: {
                form: form,
            }
        });
        modalInstance.result.then(function (result) {
        }, function () {
        });
    };
});


app.controller("modalInput", function ($state, $scope, Data, $uibModalInstance, $rootScope, form, $sce) {

    $scope.detail = form;

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});

app.controller("modalYoutube", function ($state, $scope, Data, $uibModalInstance, $rootScope, form, $sce) {

    console.log(form);
    $scope.detail = form;
    $scope.convertUrl = "https://www.youtube.com/embed/"+ form.youtube_id +"?autoplay=1&origin=https://guru.bukudigitalku.id";

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.close = function () {
        $uibModalInstance.close({
            'data': undefined
        });
    };
});
