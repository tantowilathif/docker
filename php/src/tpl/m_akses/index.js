app.controller("aksesCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef = {};
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.loading = false;


    $scope.fillProyek = function () {
        $scope.form.proyek = [];
        angular.forEach($scope.form.perusahaan, function (val, key) {
            angular.forEach($scope.listProyek, function (v, k) {
                if (v.perusahaan_id == val.id) {
                    $scope.form.proyek.push(v);
                }
            })
        });
    }

    $scope.updateProyek = function () {
        var arr = [];
        angular.forEach($scope.form.perusahaan, function (val, key) {
            angular.forEach($scope.listProyek, function (v, k) {
                if (v.perusahaan_id == val.id) {
                    arr.push(v);
                }
            })
        });
        $scope.listProyek = arr;
    }

    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("appakses/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
        $scope.isLoading = false;
    };
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.form.akses = {};
        $scope.form.is_super_admin = 0;
        if ($rootScope.user.is_super_admin != 1) {
            $scope.form.kabupaten = {
                id: $rootScope.user.desa_active.kabupaten_id,
                provinsi: $rootScope.user.desa_active.nama_provinsi,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten
            }

            $scope.form.kecamatan = {
                id: $rootScope.user.desa_active.kecamatan_id,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan
            }

            $scope.form.desa = {
                id: $rootScope.user.desa_active.m_desa_id,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan,
                desa: $rootScope.user.desa_active.desa
            }
        }
        // $rootScope.getDesa(null,'3502040',null);
    };
//    $scope.create();
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        if ($rootScope.user.is_super_admin != 1) {
            $scope.form.kabupaten = {
                id: $rootScope.user.desa_active.kabupaten_id,
                provinsi: $rootScope.user.desa_active.nama_provinsi,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten
            }

            $scope.form.kecamatan = {
                id: $rootScope.user.desa_active.kecamatan_id,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan
            }

            $scope.form.desa = {
                id: $rootScope.user.desa_active.m_desa_id,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan,
                desa: $rootScope.user.desa_active.desa
            }
        }else{
            $scope.getKecamatan(form.kabupaten.id);
            $rootScope.getDesa(null,form.kecamatan.id, null)
        }
        // $rootScope.getDesa(null,'3502040',null);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("appakses/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        var data = angular.copy(row);
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                data.is_deleted = 1;
                Data.post("appakses/saveStatus", data).then(function (result) {
                    if (result.status_code == 200) {
                        $scope.cancel();
                    } else {
                        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                    }
                });
            }
        });
    };
    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post("appakses/saveStatus", row).then(function (result) {
                    $scope.cancel();
                });
            }
        });
    };

    $scope.checkAll = function (module, value) {
        $('.' + module + ' input[type="checkbox"]').each(function (val) {
            var arr = $(this).attr('ng-model').split(".");
            $scope.form.akses[arr[arr.length - 1]] = value;
        })
    }


    $scope.getKecamatan = function (kabupaten_id) {
        Data.get('get_data/kecamatan', {kabupaten_id : kabupaten_id}).then(function (response) {
            $scope.listKecamatan = response.data.list;
        })
    }
});