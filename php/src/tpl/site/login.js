app.controller("loginCtrl", function ($scope, Data, $state, $rootScope, UserService, $timeout, $location) {
    $scope.authError = null;
    $scope.form = {};
    var user = UserService.getUser();
    if (user === null) {
    } else {
        $location.path('/dashboard');
    }
    $scope.login = function (form) {
        $scope.authError = null;
        Data.post("site/login", form).then(function (result) {
            if (result.status_code != 200) {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            } else {
                UserService.setUser(result.data.user);
                $rootScope.user = UserService.getUser();
                // $location.path('/dashboard');

                Data.get("m_setting_aplikasi/getLogo").then(function (response) {
                    $rootScope.setting_logo = response.data.list;
                });

                $state.go("app.artikel");
            }
        });
    };
});