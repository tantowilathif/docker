app.controller("userCtrl", function ($scope, Data, $rootScope, toaster, UserService) {
    /**
     * Inialisasi
     */

    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {
        password: ""
    };
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.listHakakses = [];
    $scope.loading = false;
    $rootScope.user = UserService.getUser();

    /**
     * End inialisasi
     */

    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("appuser/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };
    Data.get("get_data/hak_akses").then(function (data) {
        $scope.listHakakses = data.data;
    });

    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};

        if ($rootScope.user.is_super_admin != 1) {
            $scope.form.kabupaten = {
                id: $rootScope.user.desa_active.kabupaten_id,
                provinsi: $rootScope.user.desa_active.nama_provinsi,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten
            }

            $scope.form.kecamatan = {
                id: $rootScope.user.desa_active.kecamatan_id,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan
            }

            $scope.form.desa = {
                id: $rootScope.user.desa_active.m_desa_id,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan,
                desa: $rootScope.user.desa_active.desa
            }
        }

        // console.log("bbb");
        // toaster.pop('success', "success", "text");
        // toaster.pop('warning', "title", "text");
        // console.log("aaa");
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.password = "";
        // $rootScope.getKecamatanWithKabupaten(null,null,form.kecamatan_id,null)
        $rootScope.getDesa(null, form.kecamatan_id, null);

        if ($rootScope.user.is_super_admin != 1) {
            $scope.form.kabupaten = {
                id: $rootScope.user.desa_active.kabupaten_id,
                provinsi: $rootScope.user.desa_active.nama_provinsi,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten
            }

            $scope.form.kecamatan = {
                id: $rootScope.user.desa_active.kecamatan_id,
                kabupaten: $rootScope.user.desa_active.nama_kabupaten,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan
            }

            $scope.form.desa = {
                id: $rootScope.user.desa_active.m_desa_id,
                kecamatan: $rootScope.user.desa_active.nama_kecamatan,
                desa: $rootScope.user.desa_active.desa
            }
        }


    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.nama;
        $scope.form = form;
        $scope.form.password = "";
    };
    $scope.save = function (form) {
        $scope.loading = true;
        Data.post("appuser/save", form).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.trash = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Menghapus item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 1;
                Data.post("appuser/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };
    $scope.restore = function (row) {
        Swal.fire({
            title: 'Peringatan',
            text: 'Apa anda yakin akan Merestore item ini ?',
            type: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                row.is_deleted = 0;
                Data.post("appuser/saveStatus", row).then(function (result) {
                    $scope.displayed.splice($scope.displayed.indexOf(row), 1);
                });
            }
        });
    };

    $scope.getKecamatan = function (kabupaten_id) {
        Data.get('get_data/kecamatan', {kabupaten_id: kabupaten_id}).then(function (response) {
            $scope.listKecamatan = response.data.list;
        })
    }
});
